-- Copyright (c) 2009, Kristian Bergmann
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
--   * Redistributions of source code must retain the above copyright notice,
--     this list of conditions and the following disclaimer.
--   * Redistributions in binary form must reproduce the above copyright
--     notice, this list of conditions and the following disclaimer in the
--     documentation and/or other materials provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.


--Caches pending calling-group invite-requests in order to determine the one
--sending the actual invitation
CallingChat = {
	pendingInvites = {}
}

--This must not be localized as it is part of Calling's communication protocol.
Calling.callText = {
	L"Attack my target: ",
	L"Need assist on: "
}

--This must not be localized as it is part of Calling's communication protocol.
Calling.withdrawText = {
	L"Forget my call."
}

--This must not be localized as it is part of Calling's communication protocol.
Calling.callDeadText = {
	L" Target has been killed!"
}

--This must not be localized as it is part of Calling's communication protocol.
Calling.assistingText = {
	{ Send = L"I'm on %q's target.", Pattern = L"I'm on \"(.*)\"'s target." }
}

--This must not be localized as it is part of Calling's communication protocol.
Calling.willSendInvite = {
	{ Send = L"I will invite %q.", Pattern = L"I will invite \"(.*)\"." }
}

--id (WAR internal) of the chat channel used for calling messages
Calling.Chat = -1
--slash prefix used to send messages to the calling chat channel
Calling.ChatPrefix = nil
--the full channel name of the calling chat channel
Calling.Channel = nil
--channel number (user) of the calling chat channel
Calling.ChannelNumber = -1

--minimum seconds to pass between any two sent messages
local SEND_TIMEOUT = 3
--minimum seconds to pass between to subsequent /channelwho requests
local CHANNELWHO_TIMEOUT = 20
--seconds to wait for first user name as answer to a channelwho request
local WAIT_FOR_FIRST_USER_TIMEOUT = 10
--seconds to wait for the next user name after first one arrived.
--after this time user list will be considered complete
local WAIT_FOR_NEXT_USER_TIMEOUT = 1

--current list of players in the calling channel
Calling.UserList = {}
--used for collecting user names during a running /channelwho
local newUserList = {}
--states of the current channelwho request
local UpdateStates = {
	IDLE = 1, --no request running
	WAITING_FOR_FIRST = 2, --request running, waiting for first name to arrive
	WAITING_FOR_NEXT = 3 --request running, waiting for subsequent names
}
--time at which the last name of the current channelwho request arrived
local lastUserListStateChange = 0
--the state of the current /channelwho request (if IDLE, no request running)
local userListUpdateState = UpdateStates.IDLE

--determines if a channelwho request is currently open
function Calling.IsUserListUpdating() return userListUpdateState ~= UpdateStates.IDLE end
	
--true iff user requested to join a calling channel and 
--join request has not been sent to the network
Calling.wantCallJoin = false
--true iff channeljoin request has been sent to the network and no answer
--has been received from the network
local waitJoinChannel = false
--true iff channleave request has been sent to the network and no answer has
--been received from the network
local waitLeaveChannel = false
--name of the calling/chat channel the user has requested to join
local NewCallingChannel = nil
--time at which the last channel join command has been send to the network
local lastJoinTrial = 0
--number of trials to join NewCallingChannel since the user requested to join
local joinTrials = 0


--messages which have been requested for sending but were postponed in order
--not to trigger WARs spam blocker
local pendingSends = {}
--time at which the last message has been send, used to postpone sends for not
--triggering WARs spam blocker
local lastSend = 0
--each known user is stored in here. Used to collect users which dont send
--join/part messages
local userKnown ={}

--sends queued messages and
-- manages the waiting procedure for /calljoin answers
function Calling.ManageChat()
	--send pending messages
	if #pendingSends > 0 and
		Calling.relativeTime - lastSend > SEND_TIMEOUT
	then
		local send = pendingSends[#pendingSends]
		pendingSends[#pendingSends] = nil
		
		local channel = send.chan
		local message = send.msg

		SystemData.UserInput.ChatChannel    = towstring(channel);
		SystemData.UserInput.ChatText       = towstring(message);
		BroadcastEvent( SystemData.Events.SEND_CHAT_TEXT);
		
		lastSend = Calling.relativeTime
	end

	--timeout waiting for further users after /channelwho
	if userListUpdateState == UpdateStates.WAITING_FOR_NEXT and 
		Calling.relativeTime - lastUserListStateChange > WAIT_FOR_NEXT_USER_TIMEOUT
	then
		userListUpdateState = UpdateStates.IDLE
		lastUserListStateChange = Calling.relativeTime
		Calling.UserList = newUserList
		leftList = {}
		Calling.UpdateUserListTooltip()
		CallingSetup.UpdateDropdowns()
	end
	
	--timeout waiting for the first user after /channelwho
	if userListUpdateState == UpdateStates.WAITING_FOR_FIRST and
		Calling.relativeTime - lastUserListStateChange >  WAIT_FOR_FIRST_USER_TIMEOUT
	then
		Calling.RequestUserList()
	end
end

--queues a message for sending
function Calling.SendChat(channel, message)
--Calling.d("send: ", towstring(channel).." "..towstring(message))
	pendingSends[#pendingSends + 1] = { chan=channel, msg=message }
end

--requests a userlist of the calling channel
function Calling.RequestUserList()
	if Calling.ChannelNumber >= 0 and
		Calling.relativeTime - lastUserListStateChange > CHANNELWHO_TIMEOUT
	then
		newUserList = {}
		leftList = {}
		userKnown = {}
		Calling.SendChat(L"/channelwho", towstring(Calling.ChannelNumber))
		userListUpdateState = UpdateStates.WAITING_FOR_FIRST
		lastUserListStateChange = Calling.relativeTime
		CallingChat.wantUserlistUpdate = false
	end
end

--adds a user to the userlist of the calling channel
function Calling.AddUser(newUser)
	userKnown[newUser] = true
	
	--determine which list to add the user to
	local list  = Calling.UserList
	if userListUpdateState == UpdateStates.WAITING_FOR_FIRST or
		userListUpdateState == UpdateStates.WAITING_FOR_NEXT
	then
		list = newUserList
	end
	
	--determine if the user already is in the list
	local exists = false
	for i, user in pairs(list) do
		if user == newUser then exists = true end
	end
	
	--add the user if she is not already in the list
	if not exists then
		list[#list + 1] = newUser
		Calling.UpdateUserListTooltip()
		CallingSetup.UpdateDropdowns()
	end
end

local leftList = {}
--removes a user from the userlist of the calling channel
function Calling.RemoveUser(leavingUser)
	--determine which user list to update
	local list  = Calling.UserList
	if userListUpdateState == UpdateStates.WAITING_FOR_FIRST or
		userListUpdateState == UpdateStates.WAITING_FOR_NEXT
	then
		list = newUserList
		leftList[leavingUser] = true
	end
	
	--search for the user to be removed
	local id = nil
	for i, user in pairs(list) do
		if user == leavingUser then id = i end
	end
	
	--remove the user if she is in list
	if id then
		for i = id, #list - 1 do
			list[i] = list[i + 1]
		end
		list[#list] = nil

		Calling.UpdateUserListTooltip()
		CallingSetup.UpdateDropdowns()
	end
	userKnown[leavingUser] = nil
end

--manages leaving and joining the calling channel
function Calling.ManageChannelUpdate()
	if waitJoinChannel or waitLeaveChannel then
		local chans = ChatSettings.Channels
		local wChan = towstring(Calling.Channel)
		--iterate over all user chat channels to see if we left/joined the calling channel
		for i=57,67 do
			--manage channeljoin
			if chans[i] ~= nil and waitJoinChannel then
				local channelName = towstring(chans[i].labelText):upper()
				local cc = towstring(Calling.Channel):upper()
				local first, last = channelName:find(cc)
				if first ~= nil and Calling.Chat == -1 then
					--set variables used for channel identification and access
					Calling.Channel = towstring(chans[i].labelText:sub(first, last))
					Calling.Chat = i
					Calling.ChannelNumber = i-56
					Calling.ChatPrefix = L"/"..Calling.ChannelNumber
					d(L"channel found:"..Calling.ChannelNumber..L" | "..Calling.Channel)
					waitJoinChannel = false

					--manage changes to the ui
					Calling.SetListShown(true)
					Calling.RequestUserList()
					CallingChat.SetChannelFiltering(i, true)
					DynamicImageSetTexture( "CallingIcon",  "calling_active", 0, 0 )
					
					--send join message to the calling channel
					local career = GameData.Player.career.line
					Calling.SendChat(Calling.ChatPrefix, L"+"..towstring(career)..L"+")
				end
			end
			
			--retry joining channel a few times and break up after some unsuccessfull trials
			if waitJoinChannel and Calling.relativeTime - lastJoinTrial > SEND_TIMEOUT * 1.5 then
				if joinTrials < 5 then
Calling.d(L"REJOIN ATTEMPT: ", Calling.Channel)
					Calling.SendChat(L"/channeljoin", Calling.Channel)
					lastJoinTrial = Calling.relativeTime
					joinTrials = joinTrials + 1
				else
Calling.d(L"ABORTING JOIN: ", Calling.Channel)
					waitJoinChannel = false
					Calling.Channel = nil
					NewCallingChannel = nil
				end
			end

			--manage channelleave
			if waitLeaveChannel and i == Calling.Chat and
			  (chans[i] == nil or chans[i].labelText:find(wChan) == nil)
			then
				CallingChat.SetChannelFiltering(Calling.ChannelNumber + 56, false)

				--unset all variables indicating a calling channel
				Calling.Chat = -1
				Calling.ChannelNumber = -1
				Calling.Channel = nil
				Calling.ChatPrefix = nil
				d(L"channel left")
				waitLeaveChannel = false

				--apply ui changes
				Calling.SetListShown(false)
				Calling.UserList = {}
				CallingSetup.UpdateDropdowns()
				DynamicImageSetTexture( "CallingIcon",  "calling_inactive", 0, 0 )
				Calling.lastCall = nil
				while Calling.GetFirstInList() do
					Calling.RemoveFromCallsList(0)
				end
			end
		end
	end

	--upadte user list of calling channel if requested
	if CallingChat.wantUserlistUpdate then
		Calling.RequestUserList()
	end
end

local sinceQuit = 0
--sends a channel quit message to the calling channel (not yet leaving it)
--when receiving this message the calling channel is left
function Calling.QuitChannel()
	if (Calling.Chat ~= -1) then
		Calling.SendChat(Calling.ChatPrefix, "--")
	end
end

--sends a message to the system in order to leave the calling channel
--additionally starts waiting for the systems approval of leaving
function Calling.Quit2()
	Calling.SendChat(L"/channelleave", towstring(Calling.Channel))
	Calling.Print(towstring(Calling.GetLocalized("chanLeft"))..towstring(Calling.Channel))
	waitLeaveChannel = true
	sinceQuit = 0
end

--joins the calling-channel given in the argument
--slash command handler for /calljoin command
function Calling.RequestJoin(args)
	local wordNum = 0
	local words = {}
	
	local word = towstring(args):match(L"%w+")
Calling.d(L"WORD: ", word)

	if (word == nil) then
		Calling.Print(Calling.GetLocalized("calljoinUsage"))
		return
	end
	NewCallingChannel = towstring(word)
	
	Calling.wantCallJoin = true
end

--called to execute an action leading closer to really joining the desired
--calling channel. leaves the previous calling channel first, if necessary.
--after successfull sending of the channeljoin message, the program waits
--for the systems approval of finished channeljoin.
function Calling.JoinChannel(elapsedTime)
	--leave previous channel
	if Calling.Channel ~= nil then
		if not waitLeaveChannel then
			--request quitting currentChannel
--d("join: quit")
			Calling.QuitChannel()
			waitLeaveChannel = true
		else
			--wait unti channel is left
--d("join: wait")
		end
		return
	end
	
	--now we can finally join
	Calling.wantCallJoin = false
	Calling.Channel = NewCallingChannel
	
	d(L"CALLJOIN")
	Calling.SendChat(L"/channeljoin", Calling.Channel)
	Calling.Print(Calling.GetLocalized("chanJoined")..Calling.Channel)
	waitJoinChannel = true
	joinTrials = 1
	lastJoinTrial = Calling.relativeTime
end

--examines and interpretes the last message (from the calling channel)
--takes appropriate actions if a match is found
function Calling.ExamineCallMessage()
	--store relevant data in local variables
	local text = GameData.ChatData.text
	local textStr = towstring(text)
	local sender = GameData.ChatData.name
	local parsed = false --determines if to go on searching for message type

	--remove old invitation requests
	for toInvite, val in pairs(CallingChat.pendingInvites) do
		if Calling.relativeTime - val.startTime  > 30 then
			CallingChat.pendingInvites[toInvite] = nil
		end
	end
	
	if sender ~= nil and sender ~= L"" then
		local strippedSender = Calling.StripString(sender)

		--TODO maybe remove (userKnown) after some weeks:
		--add all writers in channel in order to catch pre 0.7.2 users
		if not userKnown[strippedSender] then
			Calling.AddUser(strippedSender)
		end
		--
		
--d(sender, text)
		--see if this is a call announcment
		local targetName, targetCareer, callerCarreer, rest
		--iterate over all versions of the call text for backwards compatibility
		for i = 1, #Calling.callText do 
			if targetName == nil then --not yet found matching version of calling text
				local callPattern = Calling.callText[i]..L"(.+) %((.+)%)(.*)"
				targetName, targetType, rest = text:match(callPattern)
--d(rest)
				if rest ~= nil then --match, target career in message
					targetCareer, temp = rest:match(L"%[(.*)%](.*)")
					if targetCareer ~= nil then targetCareer = tonumber(targetCareer) end
					if temp ~= nil then rest = temp end
				end
--d(rest)
				if rest ~= nil then --match, additionally caller career is given
					callerCareer, temp = rest:match(L"-(.*)-(.*)")
					if callerCareer ~= nil then callerCareer = tonumber(callerCareer) end
					if temp ~= nil then rest = temp end
				end
			end
		end
		
		--if this is a call, create the respective data structure and add the call to lists
		if targetName ~= nil then
			--create call
			Calling.lastCall = {
				Caller = Calling.StripString(sender),
				Target = Calling.StripString(targetName),
				TargetIsPlayer = false,
				TargetIsMonster = false,
				TargetCareerID = targetCareer,
				CallerCareerID = callerCareer
			}
			local call = Calling.lastCall

			--set target type
			if targetType == L"M" then
				call.TargetIsPlayer = false
				call.TargetIsMonster = true
			elseif targetType == L"P" then
				call.TargetIsMonster = false
				call.TargetIsPlayer = true
			end
			
			Calling.AddCall(call)
--d(L"Call from "..call.Caller..L" received")
			
			Calling.ShowNotification(call)
			parsed = true
		end 
		
		--see if this is a call withdraw
		if not parsed then
			local previousFirst = Calling.GetFirstInList()
			
			--remove withdrawn call, if message matches pattern
			if text:find(Calling.withdrawText[1]) == 1 then
--d(L"Withdraw from "..sender..L" received")
				if text:find(Calling.callDeadText[1]) then
					if CallingSettings.RemoveDeadNPC then
						Calling.RemoveCallsFrom(sender, true)
					end
				else
					Calling.RemoveCallsFrom(sender)
				end
				parsed = true
			end
			
			--if removed call was the current one, show notification for new first call
			local newFirst = Calling.GetFirstInList()
			if newFirst ~= previousFirst then
				Calling.lastCall = newFirst
				Calling.ShowNotification(newFirst)
			end
		end
		
		--see if this is an assist announcement
		if not parsed then
			local caller = text:match(Calling.assistingText[1].Pattern)
			if caller ~= nil then
--d(L"Assist"..caller)
				Calling.RemoveCallsFrom(sender)
				--remove assist on previous assisted call
				local previousCall = Calling.assistsOn[sender]
				if previousCall ~= nil then
					previousCall.Assists = previousCall.Assists - 1
					Calling.assistsOn[sender] = nil
				end
			
				--iterate over current calls and add assister on matching call
				local first = Calling.GetFirstInList()
				local call = first
				local firstReReached = false
				while not firstReReached do
					if call.Caller == caller then
						if call.Assists == nil then
							call.Assists = 1
						else
							call.Assists = call.Assists + 1
						end
						Calling.assistsOn[sender] = call
					end
					
					call = Calling.GetNextInList()
					if call == first then
						firstReReached = true
					end
				end

				parsed = true
			end
		end
		
		--in order to only send one response to an invitation request, all channel
		--users send a code to the channel.
		--the first to send this, sends the invitation
		--Checks for this code and either sends invitation or removes request from request list
		if not parsed then
			local toInvite = textStr:match(Calling.willSendInvite[1].Pattern)
			if toInvite ~= nil then
				parsed = true
				if CallingChat.pendingInvites[toInvite] ~= nil then
					if sender == GameData.Player.name and 
						CallingChat.pendingInvites[toInvite] ~= nil
					then
						CalljoinGUI.BroadcastChannel(L"/w "..towstring(toInvite))
					end
					CallingChat.pendingInvites[toInvite] = nil
				end
			end
		end
		
		--see if this is a channeljoin message, add user if so
		if not parsed then
			local joinedCareer = textStr:match(L"%+(.*)%+")
			if joinedCareer ~= nil then
				local newUser = Calling.StripString(sender)
				Calling.AddUser(newUser)
				parsed = true
			end
		end
		
		--see if this is a channelleave message, remove user if so
		if not parsed then
			local leftCareer = textStr:match(L"%-(.*)%-")
			if leftCareer ~= nil then
				local leftUser = Calling.StripString(sender)
				Calling.RemoveUser(leftUser)
				
				if leftUser == Calling.playerName then
					Calling.Quit2()
				end
				
				parsed = true
			end
		end

	else
		--see if this is a channelwho answer
		local name = text:match(L"%[(.*)%]")
		if name ~= nil and
			(userListUpdateState == UpdateStates.WAITING_FOR_FIRST or
			userListUpdateState == UpdateStates.WAITING_FOR_NEXT)
		then
--d("user")
			--first response, initialize temporary user list
			if userListUpdateState == UpdateStates.WAITING_FOR_FIRST then
				leftList = {}
				userListUpdateState = UpdateStates.WAITING_FOR_NEXT
			end

			--add the new user to the temporary list
			local addition = Calling.StripString(name)
			if not leftList[addition] then
				Calling.AddUser(addition)
			end
			lastUserListStateChange = Calling.relativeTime
		end
	end
end

--checks a message for matching a localized chat protocol pattern
--returns true iff the message matches the pattern in the given language
function Calling.MessageMatchesLocalizationPattern(msg, languageId, localizationFieldName)
	local loc = Calling.localization[languageId]
	if loc ~= nil then
		local parseAgainst = loc[localizationFieldName].pat
--d(msg)
--d(parseAgainst)
		return msg:match(parseAgainst)
	end
end

--returns true iff the message is a group management message in the given localization
--executes the appropriate program reaction to the found group management message
function Calling.ParseForGroupManagement(strippedSender, msgText, langId)
	local ct = GameData.ChatData.type
	local cf = SystemData.ChatLogFilters
	--look for invites
--	d(L"join call check")
	local channel = Calling.MessageMatchesLocalizationPattern(msgText, langId, "channelAdvertise")
	if channel ~= nil and strippedSender ~= Calling.playerName then
--d(L"join call recived")
		local inGroup = (Calling.Channel ~= nil)
		if CallingSettings.ShowGroupInvites and
			(not inGroup or not CallingSettings.SupressInvitesWhenGrouped)
		then
			CalljoinGUI.ShowInvitation(strippedSender, channel)
		end
		return true
	end
	
	--look for invitation requests
	local match = Calling.MessageMatchesLocalizationPattern(msgText, langId, "channelRequest")
	if (match ~= nil and Calling.Channel ~= nil) and
		( (CallingSettings.ReactOnInvitationRequestsPnW and (ct == cf.GROUP or ct == cf.BATTLEGROUP)) or
		(CallingSettings.ReactOnInvitationRequestsGnA and (ct == cf.GUILD or ct == cf.ALLIANCE)) or
		(CallingSettings.ReactOnInvitationRequestsL and ct == cf.SAY) )
	then
--d(L"send inv")
		local sender = towstring(strippedSender)
		local msg = Calling.willSendInvite[1].Send:format(sender)
		CallingChat.pendingInvites[sender] = { startTime = Calling.relativeTime }
		Calling.SendChat(Calling.ChatPrefix, msg)
		return true
	end

	--nothing found
	return false
end

--examines the last chat message and delegates it to the correct message handler
--if the message comes from an "interesting" channel
function Calling.ExamineChatMessage()
--d("CHAT: ", GameData.ChatData.type, GameData.ChatData.text)
	local ct = GameData.ChatData.type
	local cf = SystemData.ChatLogFilters
	if ct == Calling.Chat then
		--examine calling protocol message
		Calling.ExamineCallMessage()
	elseif ct == cf.GROUP or ct == cf.BATTLEGROUP or
		ct == cf.GUILD or ct == cf.ALLIANCE or
		ct == cf.SAY or ct == cf.TELL_RECEIVE
	then
		--check group management message against all localizations
		local text = GameData.ChatData.text
		local textStr = towstring(text)
		local sender = Calling.StripString(GameData.ChatData.name)

		local activeLang = SystemData.Settings.Language.active
		local msgParsed = Calling.ParseForGroupManagement(sender, textStr, activeLang)
--d(msgParsed)
		for lang, langId in pairs(SystemData.Settings.Language) do
			if not msgParsed and langId ~= activeLang then
				msgParsed = Calling.ParseForGroupManagement(sender, textStr, langId)
			end
		end
	elseif ct == 6 then --channeljoin and channelleave notifications
		--check for leavers that are in the calling channel in order to
		--catch disconnected callers or users of pre 0.7.2 Calling
		local usrTxt = GameData.ChatData.text:match(L"%[(.*)%](.*)")
		if usrTxt ~= nil and usrTxt ~= L"" then
			local user = Calling.StripString( usrTxt )
			--only update user list, if the leaving user is registered user in callig channel
			for i = 1, #Calling.UserList do
				if Calling.UserList[i] == user then
d(L"updating: potentially disconnected user")
					CallingChat.wantUserlistUpdate  = true
				end
			end
		end
	end
end

--toggles filtering of the given message type to the desired state in all chat tabs
function CallingChat.SetChannelFiltering(filterId, wantToFilter)
	for idx=1, #ChatSettings.Ordering do
		local curFilterId = ChatSettings.Ordering[idx]
		if filterId == curFilterId then
			for tabId, tabData in pairs (EA_ChatTabManager.Tabs) do
				if tabData.used then
					local logDisplayName = tabData.name.."TextLog"
					local logName = ChatSettings.Channels[filterId].logName
					local active = LogDisplayGetFilterState( logDisplayName, logName, filterId)
					
					--save state before joining a calling group and restore it afterwards
					if wantToFilter then
						tabData.activeBeforeCalling = active
					else
						wantToFilter = not tabData.activeBeforeCalling
					end
					
					if active == wantToFilter then
						LogDisplaySetFilterState( logDisplayName, logName, filterId, not active)
					end
				end
			end
		end
	end
    
	EA_ChatWindow.SaveSettings()	
end
