-- Copyright (c) 2009, Kristian Bergmann
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
--   * Redistributions of source code must retain the above copyright notice,
--     this list of conditions and the following disclaimer.
--   * Redistributions in binary form must reproduce the above copyright
--     notice, this list of conditions and the following disclaimer in the
--     documentation and/or other materials provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

LibCombatLog = {
	lastTimestamp = nil,
	parsedWithLastTs = 0,
	
	listenersByType = {},
	numListenersByType = 0,
	
	--TODO registering and handling of fulls
	fullListeners = {},
	numFullListeners = 0
}

--This method forwards all combat log messages to the listeners registered
--for the message's type. 
--It is not sufficient to register for the new message event as this seems
--not to be triggered for each message but for each server tick in which.
--In order to identify already parsed messages the number of parsed messages
--for each timestamp (second) are stored. The log is rewinded to the last
--parsed entry. From there, new messages can be forwarded and the last
--forwarded entry stored.
function LibCombatLog.OnUpdate(elapsedTime)
	--determine last combat log entrie's id
	local lastId = TextLogGetNumEntries("Combat") - 1
	if lastId < 0 then
		return
	end
	
	--set convenience variables
	local id = lastId	
	local timestamp, type, text = TextLogGetEntry("Combat", id)
	local currentTimestamp = timestamp
	
	--only update after a full second has passed and if listerners
	--are registered
	if  timestamp ~= nil and
		LibCombatLog.lastTimestamp ~= currentTimestamp and
		(LibCombatLog.numListenersByType > 0 or
		LibCombatLog.numFullListeners > 0)
	then
		local parsedWithTs = 0
		
		-- rewind log until we find lastTimestamp
		while timestamp ~= LibCombatLog.lastTimestamp and id >= 1 do
			if timestamp == currentTimestamp then
				parsedWithTs  = parsedWithTs + 1
			end
			id = id - 1
			timestamp, type, text = TextLogGetEntry("Combat", id)
		end
		
		--rewind to the first message with lastTimestamp
		while timestamp == LibCombatLog.lastTimestamp and id >= 1 do
			id = id - 1
			timestamp, type, text = TextLogGetEntry("Combat", id)
		end
		id = id + 1
		-- now we have the id of the first entry with lastTimestamp

		--skip all entries we already parsed
		for i = 0, LibCombatLog.parsedWithLastTs do
			id = id +1
		end		
		
		--forward all combat log entries to the appropriate listeners
		while id <= lastId do
			timestamp, type, text = TextLogGetEntry("Combat", id)
			id = id + 1
			local listeners = LibCombatLog.listenersByType[type]

			if listeners ~= nil then
				local elem = listeners.First
				while elem ~= nil do
					elem.Data(timestamp, type, text)
					elem = elem.Next
				end
			end
		end

		--remember number of parsed entries with current timestamp
		LibCombatLog.parsedWithLastTs = parsedWithTs
	else
		LibCombatLog.parsedWithLastTs = 0
	end
	
	--remember parsed timestamp
	LibCombatLog.lastTimestamp = currentTimestamp
end

--registers the given callback function for all combat log entries with
--the given type.
function LibCombatLog.RegisterForMessageType(type, fun)
	local byType = LibCombatLog.listenersByType[type]
	--create the listener list if necessary
	if byType == nil then
		LibCombatLog.listenersByType[type] = LinkedList.Create()
		byType = LibCombatLog.listenersByType[type]
	end
	
	--add callback function to listeners
	if not byType:Contains(fun) then
		LibCombatLog.numListenersByType =
			LibCombatLog.numListenersByType + 1
		byType:PushFront(fun)
	end
end

--unregisters the given callback function for all combat log entries with
--the given type
function LibCombatLog.UnregisterForMessageType(type, fun)
	local byType = LibCombatLog.listenersByType[type]
	if byType == nil then
		return
	elseif byType:Remove(fun) then
		LibCombatLog.numListenersByType =
			LibCombatLog.numListenersByType - 1
	end
end

--prints the amount of messages for each timestamp to the debug log,
--starting from the given message id.
function LibCombatLog.ShowTimestampCounters(fromId)
	local tsCount = {}
	local id = fromId
	--iterate over all messages starting at given id
	while id <= lastId do
		timestamp, type, text = TextLogGetEntry("Combat", id)
		id = id + 1
		if tsCount[timestamp] == nil then
			tsCount[timestamp]  = 1
		else
			tsCount[timestamp] = tsCount[timestamp] + 1
		end
	end
	
	--print results to the debug log
	if timestamp ~= LibCombatLog.lastTimestamp then
		for stamp, count in pairs(tsCount) do
			d(stamp..L" : "..count)
		end
		d(L"---")
	end
end
