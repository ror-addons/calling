-- Copyright (c) 2009, Kristian Bergmann
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
--   * Redistributions of source code must retain the above copyright notice,
--     this list of conditions and the following disclaimer.
--   * Redistributions in binary form must reproduce the above copyright
--     notice, this list of conditions and the following disclaimer in the
--     documentation and/or other materials provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

--sort of forward declaration
CallingSetup = {}

--initial setup of internal Calling variables
Calling = {
	SoundNum=216; --216

	--used to suppress flooding the chat with calls
	sinceLastCall = 50,
	--amount of seconds a call notification is shown
	callStaysFor = 10,
	--reference to the last called target
	lastCall = nil,
	
	--possible sorting criterions for incoming calls
	Priority = {
		CALLER = 1,
		CALLER_TYPE = 2,
		CALLER_CAREER = 3,
		TARGET_TYPE = 4,
		TARGET_CAREER = 5,
		CALLER_GROUP = 6,
		NONE = 7,
		Count = 7
	},

	--class archetypes which can be used for CALLER_TYPE and TARGET_TYPE
	--sorting of incoming calls	
	Archetypes = {
		MELEE_DMG = 1,
		RANGED_DMG = 2,
		HEALER = 3,
		TANK = 4,
		Count = 4
	},
	
	Grouptypes = {
		PARTY = 1,
		WARBAND = 2,
		SCENARIO_GROUP = 3,
		Count = 3
	},
	
	--number of call priorization criteria that can be applied
	NUM_PRIOS = 3,

	--time elapsed since starting the plugin
	relativeTime = 0,
	
	--table for memorizing who assists on which call
	assistsOn = {}
}

--memorizes last called monster for withdrawing a call once the target dies
local calledMonsterID = -1

--prints the ordinal values of all characters in the given string
function Calling.PrintChars(string)
	if string == nil then
		return
	end	
	
	local str = towstring(string)
	local output = ""
	for i = 1, string:len() do
		output = output..string:byte(i).." "
	end
	d(output)
end

--removes ^* from the end of strings in order to gain a unified representation
--of character names for instance
function Calling.StripString(string)
	local stripped = string
	local hatPos = string:find(L"%^")
	if hatPos then 
		stripped = string:sub(1,hatPos - 1)
	end
	return stripped
end

--initialize calling settings if no settings could be loaded from prior 
--sessions
CallingSettings = {
		--color of unclassified call notifications
		nR = 255,
		nG = 255,
		nB = 255,
		
		--color of mob call notifications
		mR = 0,
		mG = 255,
		mB = 0,

		--color of player call notifications
		pR = 255,
		pG = 0,
		pB = 0,
		
		--color of notifications of a newer but low priority call
		lowPrioR = 255,
		lowPrioG = 255,
		lowPrioB = 0,
		
		showTargetMarker = false,
		
		CallsListShown = true,
		KillsListShown = true,
		
		--which information is shown along with a call notification?
		CallerCareerShown = true,
		TargetCareerShown = true,
		NotificationCareerColor = true,
		
		--withdraw calls (for the whole group) if self called NPC is
		--registered to be dead?
		RemoveDeadNPC = true,
		--Evaluate all rvr kills in order to clear calls (locally) on
		--players that have been killed?
		--NOTE: turning this on is performance intenisive
		EvaluateRvrKills = true,

		--show a popup, if you are invited to a calling group?
		ShowGroupInvites = true,
		--supress popup when already grouped?
		SupressInvitesWhenGrouped = true,
		
		
		--send out an auto invitation on inivitation request through
		-- ...guild or alliance chat channel?
		ReactOnInvitationRequestsGnA = true,
		-- ...party or warband chat channel?
		ReactOnInvitationRequestsPnW = true,
		-- ...local chat channel
		ReactOnInvitationRequestsL = false,
		
		
		--which tab of the settings screen is shown?
		showMacroSettings = false,
		showKeybindingSettings = false,
		showPrioSettings = false,
		showVisibilitySettings = false,
		showGroupManagementSettings = false,
		showTargetingSettings = false,
		showTutorial = true,
		
		--priorities decide whether an incoming call overrides the current one
		--contains up to 3 priorities with fields {Type,Selection}.
		--Type is one of Calling.Priority
		--Selection is based on Type:
		--  Priority.CALLER: Selection is the caller's name
		--  Priority.CALLER_TYPE and Priority.TARGET_TYPE:
		--    The Calling.Archetype id.
		--  Priority.CALLER_CAREER and Priority.TARGET_CAREER:
		--    The GameData.CareerLine id
		--  Priority.CALLER_GROUP
		--    The Calling.Grouptype.id
		Priorities = {},
		
		--preferred language (-1 means active language)
		language = -1
	}


CallingInit = {}

--true iff a call should be issued in next update
local needCall = false
--true iff a target should be issued in next update
local needTarget = false

--used to decide whether to target first in list or cycle through list 
--(if time between current and last press is < TARGET_TOGGLE_TIMEOUT)
local sinceLastTarget = 0
local TARGET_TOGGLE_TIMEOUT = 1

--helper for debug output:
--outputs string concatenated with value (safe for value==nil)
function Calling.d(string, value)
	if value == nil then
		d(towstring(string))
	else
		d(towstring(string)..towstring(value))
	end
end

--prints a (CALL: prefixed) message to the chat console
function Calling.Print(txt)
    if "string" == type(txt) then
        EA_ChatWindow.Print(Calling.GetLocalized("addonname")..L": "..StringToWString(txt))
    else
        EA_ChatWindow.Print(Calling.GetLocalized("addonname")..L": "..txt)
    end
end

--creates windows, registers callback functions and sets up some internal variables
function Calling.Initialize()
	--create setup window
	CreateWindow("CallingSetup", false)
	CallingSetup.DropdownsFromPriorities()
   
	--create call notification window
	CreateWindow("CallWindow", true)
   	LayoutEditor.RegisterWindow( "CallWindow",
		Calling.GetLocalized("notificationWindowCaption"),
		Calling.GetLocalized("notificationWindowDescription"),
		false, false,
		true, nil )

	--create target marker sign
	CreateWindow("CallingTargetMarker", false)
	Calling.SetTargetAlpha(0)
	Calling.SetShowMarker(CallingSettings.showTargetMarker)

	--create list of most recent calls
	CreateWindow("CallingList", true)
   	LayoutEditor.RegisterWindow( "CallingList",
		Calling.GetLocalized("listWindowCaption"),
		Calling.GetLocalized("listWindowDescription"),
		false, false,
		true, nil )
	CallingInit.CallsHeader = { Caller = Calling.GetLocalized("caller"), Target = Calling.GetLocalized("target") },
	Calling.SetListLine(CallsList, 0, CallingInit.CallsHeader )
		
	--create list of most recent kills
	CreateWindow("KillsList", true)
   	LayoutEditor.RegisterWindow( "KillsList",
		Calling.GetLocalized("killsWindowCaption"),
		Calling.GetLocalized("killsWindowDescription"),
		false, false,
		true, nil )
	CallingInit.KillsHeader = { Caller = Calling.GetLocalized("caller"), Target = Calling.GetLocalized("killedTarget") }
	Calling.SetListLine(KillsList, 0, CallingInit.KillsHeader )

	--initialize calls and kills lists
	Calling.ManageListsShowing()
	
	--create calling icon which is used to manage calling groups and open setup
	CreateWindow("CallingIcon", true)
   	LayoutEditor.RegisterWindow( "CallingIcon",
		Calling.GetLocalized("callingIconCaption"),
		Calling.GetLocalized("callingIconDescription"),
		false, false,
		true, nil )
	
	--create group management window
	CreateWindow("CalljoinGUIWindow", false)
	
	--register events
	RegisterEventHandler(SystemData.Events.CHAT_TEXT_ARRIVED, "Calling.ExamineChatMessage")
	RegisterEventHandler( SystemData.Events.SCENARIO_BEGIN, "Calling.OnScenarioUpdate")
	RegisterEventHandler( SystemData.Events.SCENARIO_UPDATE_POINTS, "Calling.OnScenarioUpdate")
	RegisterEventHandler( SystemData.Events.SCENARIO_END, "Calling.OnScenarioEnd" )
	RegisterEventHandler(SystemData.Events.PLAYER_TARGET_UPDATED, "Calling.UpdateTargets")

	--setup internal variables
	Calling.SetEvaluateRvrKills(CallingSettings.EvaluateRvrKills)
	Calling.playerName = Calling.StripString( towstring(GameData.Player.name) )
	
	Calling.Print(Calling.GetLocalized("callInited"))
	
	--initialize new settings
	if CallingSettings.NotificationCareerColor == nil then
		CallingSettings.NotificationCareerColor = true
	end
end

local libSlashChecked = false
-- helper. checks for installation of libslass and
-- initializes slash-commands if libslash is found.
function Calling.TryRegisterSlashCommands()
	if not LibSlash then
		Calling.Print(Calling.GetLocalized("noLibSlashFeatures"))
	else	
		LibSlash.RegisterWSlashCmd(L"calljoin", function(args) Calling.RequestJoin(args) end)
		LibSlash.RegisterWSlashCmd(L"callquit", function(args) Calling.QuitChannel(args) end)
		LibSlash.RegisterWSlashCmd(L"calling", function() CallingSetup.Show()  end)
		LibSlash.RegisterWSlashCmd(L"callmark", function() Calling.SetShowMarker(not CallingSettings.showTargetMarker) end)
		
		Calling.Print(Calling.GetLocalized("callSlashInited"))
	end
	
	libSlashChecked = true
end

-- update method, called each frame.
-- updates the plugin logic and issues chat commands.
--
-- NOTE: Calling chat entries need to be issued here,
-- as chat commands will not be issued if directly called by
-- a slash command handler.
function Calling.OnUpdate(elapsedTime)
	if not libSlashChecked then
		Calling.TryRegisterSlashCommands()
	end

	Calling.relativeTime = Calling.relativeTime + elapsedTime

	--show setup (in order to create Call and Target macros) if one of the
	--calling macros has been created
	if ( not GetMacroId(L"/script Calling.Call()") ) or ( not GetMacroId(L"/script Calling.Target()")) then
		CallingSetup.Show()
	end
    
	if CallingSettings.showTargetMarker then
		Calling.UpdateTargetMarker(elapsedTime)
	end

	--check if sending of chat messages has been requested and send, if so 
	if Calling.wantCallJoin then
		Calling.JoinChannel()
	end
	if needCall then
		needCall = false
		Calling.ExecuteCall()
	end
	if needTarget then
		needTarget = false;
		Calling.ExecuteTarget()
	end

	--let the different modules do their update-work
	Calling.ManageNotification(elapsedTime)
	Calling.ManageChannelUpdate()
	Calling.ManageChat(elapsedTime)	
	Calling.ManageList(elapsedTime)	
	Calling.ManageAssists(elapsedTime)
end

--helper variables for Calling.ManageAssists
local currentlyAssistedCall = nil
local assistingSince = 0
local lastBroadcastedAssist = nil
local lastAssistBroadcast = 0
local ASSIST_BROADCAST_TIMEOUT = 5
--this method decides when to issue a chat message telling the other players
--on which target the player currently assists
function Calling.ManageAssists(elapsedTime)
	sinceLastTarget = sinceLastTarget + elapsedTime
	
	local rT = Calling.relativeTime
	--conditions for announcing current target
	if currentlyAssistedCall ~= nil and --we currently are assisting
		currentlyAssistedCall ~= lastBroadcastedAssist and --we have not yet announced the current assist target
		rT - assistingSince > 1.5 * TARGET_TOGGLE_TIMEOUT and --the current assist target has been locked on for a while
		rT - lastAssistBroadcast > ASSIST_BROADCAST_TIMEOUT --time since the last assist message is long enough
	then
		lastBroadcastedAssist = currentlyAssistedCall
		local call = currentlyAssistedCall
		lastAssistBroadcast = rT
		
		local callMessage = towstring(Calling.assistingText[1].Send):format(towstring(call.Caller))
		Calling.SendChat(Calling.ChatPrefix, callMessage)
	end
end

--withdraws a previously called NPC once it is dead
function Calling.UpdateTargets(targetClass, targetId, targetType)
	if (targetClass == "selfhostiletarget" or targetClass == "mouseovertarget") and
		TargetInfo:UnitEntityId(targetClass) == calledMonsterID and
		TargetInfo:UnitHealth(targetClass) == 0
	then
		Calling.WithdrawTarget(true)
		calledMonsterID = -1
	end
end

-- Scenario handling variables
Calling.preScenarioChannel = nil
Calling.inScenario = false
--joins a scenario-specific calling channel (until scenario ends)
function Calling.OnScenarioUpdate()
--d("scenarioupdate")
	if not Calling.inScenario then
		Calling.preScenarioChannel = Calling.Channel
		local scenarioId = GameData.ScenarioData.id
		Calling.RequestJoin( L"SZ"..scenarioId..L"R"..GameData.Player.realm )
		Calling.inScenario = true
	end
end

--exists the scenario specific calling channel and rejoins the previous one,
--if there was one
function Calling.OnScenarioEnd()
--d("scenarioend")
	if Calling.preScenarioChannel then
		Calling.RequestJoin( Calling.preScenarioChannel )
	else
		Calling.QuitChannel()
	end
	Calling.inScenario = false
end

-- Calling

--those methods tell the system to send out a call in next update
function Calling.RequestCall()	
	needCall = true
end
function Calling.Call()
	needCall = true
end

--this method sends out a call message via chat.
--player needs to be in a calling group for this. 
--additionally she needs a target which is not dead.
function Calling.ExecuteCall()
	calledMonsterID = -1
	
	if Calling.Chat == -1 then
		Calling.ShowBlinkingText(Calling.GetLocalized("useCalljoin"))
		PlaySound( GameData.Sound.APOTHECARY_FAILED)
		return
	end
	
	local target = TargetInfo:UnitName("selfhostiletarget")
	local isDead = (TargetInfo:UnitHealth("selfhostiletarget") == 0)
	if target ~= nil and target ~= L"" and not isDead then
		local callMessage = Calling.callText[1]..target
		local isNPC = TargetInfo:UnitIsNPC("selfhostiletarget")
		local callerCareerId = GameData.Player.career.line

		if isNPC then
			callMessage = callMessage..L" (M)-"..callerCareerId..L"-"
			calledMonsterID = TargetInfo:UnitEntityId("selfhostiletarget")
		else
			local targetCareerId = TargetInfo:UnitCareer( "selfhostiletarget" )
			callMessage =callMessage..L" (P)["..targetCareerId..L"]-"..callerCareerId..L"-"
		end
--d("executing call: "..towstring(callMessage))
		Calling.SendChat(Calling.ChatPrefix, callMessage)
	else
		Calling.WithdrawTarget(false)
	end
end

--withdraws the last call and adds a text to tell that the target died if this
--is the case.
function Calling.WithdrawTarget(isDead)
	local message = Calling.withdrawText[1]
	if isDead then
		message = message..Calling.callDeadText[1]
	end
--d("withdrawing call: "..towstring(message))
	Calling.SendChat(Calling.ChatPrefix, message)	
end

-- Targetting

--these messages will trigger sending a target message in the next update
function Calling.TargetCalled()
	needTarget = true
	
end
function Calling.Target()
	needTarget = true
end

--determines on which call to assist next and sets the current assist on this
--target.
function Calling.ExecuteTarget()
	if Calling.Chat == -1 then
		Calling.ShowBlinkingText(Calling.GetLocalized("useCalljoin"))
		PlaySound( GameData.Sound.APOTHECARY_FAILED)
		return
	end

	--get first from call list, if first subsequent target request,
	--toggle through list otherwise
	local assistedCall, id
	if sinceLastTarget < TARGET_TOGGLE_TIMEOUT then
		assistedCall, id  = Calling.GetNextInList()
	else
		assistedCall, id = Calling.GetFirstInList()
	end
	sinceLastTarget = 0
	
	--set assist target if one could be determined
	if assistedCall ~= nil then
		Calling.AssistOnCall(assistedCall, id)
	end
end

--marks the given call as assisted and broadcasts the assist via chat
function Calling.AssistOnCall(call, listId)
	if Calling.playerName~=call.Caller then
		Calling.MarkListEntry(CallsList, listId)
		SystemData.UserInput.ChatText = towstring(L"/assist " .. call.Caller)
		BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
		currentlyAssistedCall = call
	end
end

--returns true iff the given careerLine fits the Calling.Archetypes type
function Calling.IsCareerOfType(careerLine, type)
	local cl = GameData.CareerLine

	if type == Calling.Archetypes.MELEE_DMG and
		(careerLine == cl.WARRIOR or careerLine == cl.SEER or
		careerLine == cl.ASSASSIN or careerLine == cl.WITCH_HUNTER or
		careerLine == cl.HAMMERER or careerLine == cl.CHOPPA )
	then
		return true
	elseif type == Calling.Archetypes.RANGED_DMG and
		(careerLine == cl.ENGINEER or careerLine == cl.MAGUS or
		careerLine == cl.SHADOW_WARRIOR or careerLine == cl.SQUIG_HERDER or
		careerLine == cl.SORCERER or careerLine == cl.BRIGHT_WIZARD )
	then
		return true
	elseif type == Calling.Archetypes.HEALER and
		(careerLine == cl.ARCHMAGE or careerLine == cl.SHAMAN or
		careerLine == cl.BLOOD_PRIEST or careerLine == cl.WARRIOR_PRIEST or
		careerLine == cl.RUNE_PRIEST or careerLine == cl.ZEALOT )	
	then
		return true
	elseif type == Calling.Archetypes.TANK and
		(careerLine == cl.SHADE or careerLine == cl.IRON_BREAKER or
		careerLine == cl.KNIGHT or careerLine == cl.CHOSEN or
		careerLine == cl.SWORDMASTER or careerLine == cl.BLACK_ORC )	
	then
		return true
	else
		return false
	end
end

--return true iff the given player is in the group identified by the given Calling.Grouptype
function Calling.IsPlayerInGroup(player, grouptype)
	if grouptype == Calling.Grouptypes.PARTY then
		local groupSize = GetNumGroupmates()
		if groupSize > 0 and Calling.playerName:find(player) then
			return true
		end
		
		local groupData = GetGroupData()
		for i = 1, groupSize do
			if groupData[i].name:find(player) ~= nil then
				return true
			end
		end
		return false
	elseif grouptype == Calling.Grouptypes.WARBAND then
		local wbData = GetBattlegroupMemberData()
		for group = 1, 4 do
			for i = 1, #wbData[group].players do
				if wbData[group].players[i].name:find(player) then
					return true
				end
			end
		end
		return false
	elseif grouptype == Calling.Grouptypes.SCENARIO_GROUP then
		local players = GameData.GetScenarioPlayerGroups()
		if players == nil then
			return false
		end
		
		local ownGroup = -1
		local playerGroup = -2
		for i = 1, #players do
			if ownGroup < 0 and players[i].name:find(Calling.playerName) then
				ownGroup = players[i].sgroupindex
			end
			
			if playerGroup < 0 and players[i].name:find(player) then
				playerGroup = players[i].sgroupindex
			end
		end
		
		return ownGroup == playerGroup
	end

	--should not be reached
	return false
end

--returns the rank of the first priority the given call matches (if any)
--the higher this rank, the more urgent the call and the higher the 
--probability that it will override the current call (dependent on the
--priorities of the last calls)
function Calling.GetCallPriority(call)
	--fall through the priorities until the given call matches a
	--prioritie's criterion
	for i = 1, Calling.NUM_PRIOS do
		local priority = CallingSettings.Priorities[i]

		if priority ~= nil and priority.Selection ~= nil then
			if priority.Type == Calling.Priority.CALLER and
				towstring(call.Caller) == towstring(priority.Selection)	
			then
				return i
			elseif priority.Type == Calling.Priority.CALLER_TYPE and
				Calling.IsCareerOfType(call.CallerCareerID, priority.Selection)
			then
				return i
			elseif priority.Type == Calling.Priority.CALLER_CAREER and
				call.CallerCareerID == priority.Selection
			then
				return i			
			elseif priority.Type == Calling.Priority.TARGET_TYPE and
				call.TargetCareerID == priority.Selection
			then
				return i		
			elseif priority.Type == Calling.Priority.TARGET_CAREER and
				Calling.IsCareerOfType(call.TargetCareerID, priority.Selection)
			then
				return i
			elseif priority.Type == Calling.Priority.CALLER_GROUP and
				Calling.IsPlayerInGroup(call.Caller, priority.Selection)
			then
				return i
			end
		end
	end
	
	--fallthrough: if the call does not match any priority
	return Calling.NUM_PRIOS + 1
end

--Hooked into LibCombatLog in order to remove killed players from the
--calls list
local function KillEvaluator(_, _, txt)
	Calling.RemoveFromKillMsg(txt)
end

--turns evaluation of pvp kills on and off
--
--NOTE: turning this on may consume much cpu time 
--(and therefore make the game slower)
function Calling.SetEvaluateRvrKills(doEvaluate)
	CallingSettings.EvaluateRvrKills = doEvaluate
	
	--determine kills of which faction to evaluate
	local filter
	if GameData.Player.realm == GameData.Realm.ORDER then
		filter = SystemData.ChatLogFilters.RVR_KILLS_ORDER
	else
		filter = SystemData.ChatLogFilters.RVR_KILLS_DESTRUCTION
	end
	
	--register or unregister hook function with LibCombatLog
	if LibCombatLog ~= nil then
		if doEvaluate then
			LibCombatLog.RegisterForMessageType(filter, KillEvaluator)
		else
			LibCombatLog.UnregisterForMessageType(filter, KillEvaluator)
		end
	elseif doEvaluate then
		Calling.Print(Calling.GetLocalized("noLibCombatLogFeatures"))
	end
end
