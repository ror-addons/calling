-- Copyright (c) 2009, Kristian Bergmann
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
--   * Redistributions of source code must retain the above copyright notice,
--     this list of conditions and the following disclaimer.
--   * Redistributions in binary form must reproduce the above copyright
--     notice, this list of conditions and the following disclaimer in the
--     documentation and/or other materials provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

LinkedList = {}

--Creates an empty LinkedList
function LinkedList.Create()
	return { First = nil, Last = nil, Count = 0,
		Contains = LinkedList.Contains,
		PushFront = LinkedList.PushFront,
		PushBack = LinkedList.PushBack,
		RemoveElement = LinkedList.RemoveElement,
		Remove = LinkedList.Remove
	}
end

--Adds the given data at front of the given linked list
function LinkedList.PushFront(self, data)
	local newElem = { Prev = nil, Next = self.First, Data = data }
	self.First = newElem
	
	if self.Last == nil then
		self.Last = newElem
	else
		newElem.Next.Prev = newElem
	end
	
	self.Count = self.Count + 1
end

--Adds the given data at the end of the given linked list
function LinkedList.PushBack(self, data)
	local newElem = { Prev = self.Last, Next = nil, Data = data }
	self.Last = newElem
	
	if self.First == nil then
		self.First = newElem
	else
		newElem.Prev.Next = newElem
	end
	
	self.Count = self.Count + 1
end

--returns true iff the given list contains the given element
function LinkedList.Contains(self, data)
	local elem = self.First
	while elem do
		if elem.Data == data then
			return true
		end
		elem = elem.Next
	end	
	return false
end

--Internal: Remove the given node from the given list.
function LinkedList.RemoveElement(self, elem)
	if elem == self.First then
		self.First = elem.Next
	else
		elem.Prev.Next = elem.Next
	end
	
	if elem == self.Last then
		self.Last = elem.Prev
	else
		elem.Next.Prev = elem.Prev
	end
	
	self.Count = self.Count - 1
end

--Removes the node containing the given data from the given list
function LinkedList.Remove(self, data)
	local elem = self.First
	while elem do
		if elem.Data == data then
			self:RemoveElement(elem)
			return true
		end
		elem = elem.Next
	end	
	return false
end