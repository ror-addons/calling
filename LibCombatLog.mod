<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="LibCombatLog" version="1.0" date="02/24/2009" >

		<Author name="Narketia" email="narketia [a] gmx.net" />
		<Description text="Provides a unified and (as far as possible) performant approach to accessing combat log messages." />
		
		<Dependencies/>
        
		<Files>
			    <File name="LibCombatLog.lua" />
			    <File name="LinkedList.lua" />
		</Files>
		
		<OnInitialize >
		</OnInitialize>
		
		<OnUpdate>
			<CallFunction name="LibCombatLog.OnUpdate" />
		</OnUpdate>
		
		<OnShutdown/>
		<VersionSettings gameVersion="1.3.1" />
	</UiMod>
</ModuleFile>
