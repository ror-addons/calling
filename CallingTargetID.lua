-- Copyright (c) 2009, Kristian Bergmann
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
--   * Redistributions of source code must retain the above copyright notice,
--     this list of conditions and the following disclaimer.
--   * Redistributions in binary form must reproduce the above copyright
--     notice, this list of conditions and the following disclaimer in the
--     documentation and/or other materials provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.



--The functionality of this file is used to show a target marker above the
--current assist target. 
--Doing so includes parsing every combat log message in order to make up a
--mapping between player names and their client ids. In bigger battles this
--is very CPU intensive and might impact the game's performance heavily.



--maps between enemy names and their id
Calling.enemyNameToId = {}
Calling.enemyIdToName = {}

--maps betwenn freind names and their id
Calling.friendlyNameToId = {}
Calling.friendlyIdToName = {}

--updates the cached id for the given target (friendly, enemy, mouseover)
function Calling.UpdateCacheFromTarget(targetId)
	uid = TargetInfo:UnitEntityId(targetId)
	if uid > 0 and not TargetInfo:UnitIsNPC(targetId) then
		local isFriendly = TargetInfo:UnitIsFriendly(targetId)
		local name = TargetInfo:UnitName(targetId)

		name = Calling.StripString( towstring(name) )
		if isFriendly then
			Calling.friendlyNameToId[name] = uid
			Calling.friendlyIdToName[uid] = name
		else
			Calling.enemyNameToId[name] = uid
			Calling.enemyIdToName[uid] = name
		end		
	end
end

--called if one of the targets has changed in order to update the name<->id
--bindings
function Calling.OnTargetChanged()
	TargetInfo:UpdateFromClient()
	Calling.UpdateCacheFromTarget("mouseovertarget")
	Calling.UpdateCacheFromTarget("selffriendlytarget")
	Calling.UpdateCacheFromTarget("selfhostiletarget")	
end

--returns the name of the target affected by a combat log message (toParse)
--if it matches the given set of patterns (phrases)
function Calling.ParseAgainst(toParse, phrases)
	local enemy = nil
	for i = 1, #phrases do
		local phrase = phrases[i]
		if not enemy then
			enemy = phrase[2](toParse:match(phrase[1]))
			i = phrases
		end  
	end
	return enemy
end

local lastParsed = nil

--returns the last parsed combat message's id by matching the text of the
--last parsed message against the last 20 combat log messages.
--Returns the id of the last parsed message or -1 if last parsed could
-- not be found.
--
--This procedure seems to be necessary as the CHAT UPDATE EVENT seems
--NOT to TRIGGER FOR EACH SINGLE COMBAT MESSAGE. Instead it seems to
--trigger once for every server tick in which a combat event occured. 
function Calling.GetLastParsedId()
	local id = TextLogGetNumEntries("Combat") - 1
	local timestamp, type, text
	for offset = 0, 20 do
		if id >= 0 then
			timestamp, type, text = TextLogGetEntry("Combat", id)
			if lastParsed ~= nil and 
				lastParsed.ty == type and
				lastParsed.ts == timestamp and
				lastParsed.te == text
			then
				return id
			end
		end
		id = id - 1
	end
	
	if id >= 0 then 
		return id
	else
		return -1
	end
end

--gets a not yet parsed combat log entry that contains the given amount of 
--damage or heal.
--This is used to establish the link between combat log messages (containing
--player names) and combat events (containing client side player ids)
function Calling.GetCombatLogEntry(amount)
	if amount == 0 or amount == 1 then 
		return
	end
	
	local id = Calling.GetLastParsedId() + 1
	local maxId = TextLogGetNumEntries("Combat") - 1
	while id <= maxId do
		local timestamp, type, text = TextLogGetEntry("Combat", id)
		local pattern = L" "..towstring(-amount)..Calling.GetLocalized("dmgText")
		if text:match(pattern) then
			lastParsed = {ts=timestamp, ty=type, te=text}
			return timestamp, type, text
		end
		id = id + 1
	end
	
	return nil
end

--Establishes establish a connection between the afffected object's id and
--its name by combining it with a combat log message, if possible.
function Calling.OnCombatEvent(objectID, amount, combatEvent, abilityID)
	if objectID ~= GameData.Player.worldObjNum then
		local enemy = nil
		
		--find matching combat log entry by amount of damage or heal
		local timestamp, type, text = Calling.GetCombatLogEntry(amount)
		
		--find out name of the affected enemy by matching against
		--combat log message patterns
		if type == SystemData.ChatLogFilters.YOUR_HITS then
			enemy = Calling.ParseAgainst(text, Calling.GetLocalized("hitPhrases"))
		elseif type == SystemData.ChatLogFilters.PET_HITS  then
			enemy = Calling.ParseAgainst(text, Calling.GetLocalized("petPhrases"))
		elseif type == SystemData.ChatLogFilters.YOUR_DMG_FROM_PC then
			enemy = Calling.ParseAgainst(text, Calling.GetLocalized("dmgPhrases"))
		end
		
		if enemy ~= nil then
			--find out if the enemy name seems to be unique
			local isUnique = true
			if enemy:find(L"the ") == 1 then isUnique = false
			elseif enemy:find(L"den ") == 1 then isUnique = false
			elseif enemy:find(L"die ") == 1  then isUnique = false
			elseif enemy:find(L"das ") == 1  then isUnique = false
			elseif enemy:find(L"Eine ") == 1  then isUnique = false
			elseif enemy:find(L"Einen ") == 1  then isUnique = false end

			--if so, map between found name and event's object id
			if isUnique then
				d(L"setting (id="..objectID..L" enemy="..towstring(enemy)..L") from combat log")
				Calling.enemyNameToId[enemy] = objectID
				Calling.enemyIdToName[objectID] = enemy
			end
		end
	end
end
