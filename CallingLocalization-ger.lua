-- Copyright (c) 2009, Kristian Bergmann
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
--   * Redistributions of source code must retain the above copyright notice,
--     this list of conditions and the following disclaimer.
--   * Redistributions in binary form must reproduce the above copyright
--     notice, this list of conditions and the following disclaimer in the
--     documentation and/or other materials provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

--Definition of the german localization of the Calling addon.
--In order to define the localization for a  different language, copy this file
--and change the language identifier in the next line as well as all strings.
--Placeholders (in parantheses) need to stay in the given amount and order.
Calling.localization[SystemData.Settings.Language.GERMAN] = {
	hitPhrases = {
		{L"Ihr trefft (.+) kritisch und (.*)", function (e, r) return e end},
		{L"Ihr trefft (.+) und (.*)", function (e, r) return e end}
	},
	
	petPhrases = {
		{L"Euer Begleiter trifft (.+) kritisch und (.*)" , function (e, r) return e end},
		{L"Euer Begleiter trifft (.+) und (.*)", function (e, r) return e end}
	},

	dmgPhrases = {
		{L"(%a+) trifft Euch kritisch und (.*)", function (e, r) return e end},
		{L"(%a+) trifft Euch und (.*)", function (e, r) return e end}
	},
	killPattern = L"(.*) wurde von (.*) mit (.*).(.*)",

	addonname=L"CALL",
	notificationWindowCaption = L"Calling Benachrichtigung",
	notificationWindowDescription = L"Zeigt den letzten caller, das gecallte Ziel und die Karriere des Ziels.",
	listWindowCaption = L"Calling Liste",
	listWindowDescription = L"Zeigt eine Liste der letzten paar Calls.",
	killsWindowCaption = L"Calling-Kills Liste",
	killsWindowDescription = L"Zeigt eine Liste der letzten paar nach Calls get�teten Gegner.",
	callingIconCaption = L"Calling Icon",
	callingIconDescription = L"Per Klick aufs Icon k�nnen Calling-Gruppen verwaltet und das Calling Setup angezeigt werden",
	callInited = L"Calling initialisiert",
	noLibSlashFeatures = L"Um Slash-Kommandos von \"Calling\"  benutzen zu k�nnen installiert bitte das Plugin \"LibSlash\".",
	noLibCombatLogFeatures = L"Um im RvR get�tete Gegner automatisch aus der Call-Liste aus zu blenden, muss das Plugin \"LibCombatLog\" aktiviert sein.",
	callSlashInited = L"Slash-Befehle registriert",
	useCalljoin = L"! Nutzt zuerst die Gruppenverwaltungsoptionen des Calling Symbols !",
	callLabel = L" callt ",
	chanLeft = L"Calling-Gruppe verlassen: ",
	calljoinUsage = L"Syntax: /calljoin CallingGruppenName",
	chanJoined = L"Trete der Calling-Gruppe bei: ",
	markEnabled = L"Zielmarkierung aktiviert",
	markDisabled = L"Zielmarkierung deaktiviert",
	dmgText = L" Schadenspunkte",

	subMenus = L"Untermenus",

	macros = L"Makros",
	macroInstructions = L"Benutzt Drag&Drop um die beiden Makros in die Skillleiste zu ziehen.",

	keybindings = L"Tastenbelegung",
	boundKey = L"Belegung",
	bindSelectInstructions = L"W�hlt per Rechtsklick einen Platz in der Skillleiste, um die Tasten f�r ein Makro festzulegen.",
	bindClickInstructions = L"Klickt auf den Button unten, um die Tastenbelegung einzugeben.",
	bindInstructions = L"W�hlt jetzt die Tastenbelegung oder klickt zum Abbrechen erneut auf den Knopf.",
	noSlotSelected = L"Kein Fertigkeitsplatz ausgew�hlt.",
	selectSlot = L"Platz w�hlen",
	selectSlotToProceed = L"W�hlt einen Fertigkeitsplatz zum Fortfahren.",
	selectedSlot = L"Markierter Platz: ",
	inBar = L" in Leiste ",
	
	priorities = L"Priorit�ten",
	prioInstructions = L"Diese Priorit�ten geben an wie wichtig ein Call ist.\nHat ein Call in eine hohe Priorit�t erscheint er weit oben in der Call-Liste.\nHat er die h�chste vorhandene Priorit�t wird er zum aktuellen Call.",
	prio1 = L"H�chste Priorit�t",
	prio2 = L"2. Priorit�t",
	prio3 = L"3. Priorit�t",
	type = L"Art",
	selection = L"Wahl",

	display = L"Anzeige",
	showGroupIcons = L"Show Group Icons",
	showCallList = L"Call-Liste zeigen",
	showKillList = L"Kills-Liste ziegen",
	showCallerClass = L"Callerkarriere zeigen",
	showTargetClass = L"Zielkarriere zeigen",
	notificationCareerColor = L"Zielkarriere bestimmt Callfarbe",
	language = L"Sprache",
	languageName = L"Deutsch",
	activeLanguage = L"Aktive Sprache",
	autoEnabled = L"Auto Assisting Enabled",
	autoDisabled = L"Auto Assisting Disabled",

	targeting = L"Ziel-Optionen",
	showTargetMarker = L"Zielmarkierung zeigen (performance)",
	evaluateRvrKills = L"Spieler-Calls bei Tod entfernen (performance)",
	removeNpc = L"NPC-Calls bei Tod entfernen",

	groupMgmt = L"Gruppenverwaltung",
	autoAssist = L"Auto Assisting",
	inviteDesc = L"Dialog bei Einladung in Calling-Gruppe:",
	showGroupInvites = L"Anzeigen",
	showInvitesWhenGrouped = L"Nicht wenn in Call-Grp",
	inviteOnRequestDesc = L"Einladung auf Anfrage verschicken:",
	reactOnInvitationRequestsGnA = L"Anfrage aus Gilde+Alli",
	reactOnInvitationRequestsPnW = L"Anfrage aus Gruppe+KT",
	reactOnInvitationRequestsL = L"Anfrage aus lokalem Chat",		

	age = L"Alter",
	caller = L"Caller",
	target = L"Ziel",
	lived = L"Zeit",
	killedTarget = L"Erlegter",
	clickToAssist = L"Mausklick zum Assisten von ",
	versus = L" gegen ",
	rightClickToRemove = L"Entfernt diesen Eintrag aus der Liste.",
	updating = L"--update--",
	userList = L"Caller Liste",
	
	priorityTypes = {
		[Calling.Priority.CALLER] = L"Caller",
		[Calling.Priority.CALLER_TYPE] = L"Caller-Archetyp",
		[Calling.Priority.CALLER_CAREER] = L"Caller-Karriere",
		[Calling.Priority.TARGET_TYPE] = L"Ziel-Archetyp",
		[Calling.Priority.TARGET_CAREER] = L"Ziel-Karriere",
		[Calling.Priority.CALLER_GROUP] = L"Caller Gruppe",
		[Calling.Priority.NONE] = L"-Nichts-"
	},
	
	characterTypes = {
		[Calling.Archetypes.MELEE_DMG] = L"Nahk�mpfer",
		[Calling.Archetypes.RANGED_DMG] = L"Fernk�mpfer",
		[Calling.Archetypes.HEALER] = L"Heiler",
		[Calling.Archetypes.TANK] = L"Tank"
	},
	
	groupTypes = {
		[Calling.Grouptypes.PARTY] = L"Gruppe",
		[Calling.Grouptypes.WARBAND] = L"Kriegstrupp",
		[Calling.Grouptypes.SCENARIO_GROUP] = L"Szenariengruppe"
	},

	choosePrioType = L"W�hlt den Priorit�tstyp",
	
	version06Warning = L"CALLING: Szenarien-Callgruppen ge�ndert! Erinnert Mitstreiter Calling v0.6+ herunterzuladen, um euch zu koordinieren!",
	timePart1 = L" (vor ",
	timePart2 = L")",
	
	braggingTime = L" wurde ",
	braggingCaller = L" nach erhalt des Calls von ",
	braggingAssisters = L" mit Hilfe von ",
	braggingKilled = L" Assistern get�tet.",
	clickToInsert = L"Klicken, um folgenden Text in die Chateingabe einzuf�gen: ",
	groupManagement = L"LM: Calling-Gruppen betreten, verlassen und verwalten",
	settingsShownToggle = L"RM: Calling-Setup Fenster zeigen/verbergen",
	groupFounder = L"Calling-Gruppe: ",

	noChannelOptions = L'"Erstellen" erstellt Calling-Gruppe namens %q. Mit "Anfragen" k�nnt ihr existierende Calling-Gruppen finden.',
	cancel = L"Abbrechen",
	request = L"Anfragen",
	create = L"Erstellen",
	inChannelOptions = L'Mit "Einladen" k�nnt ihr Mitstreiter in eure Calling-Gruppe einladen. "Verlassen" verl�sst eure Calling-Gruppe.',
	invite = L"Einladen",
	leave = L"Verlassen",
	inviteInstructions= L"W�hlt in der Dropdownliste den Chat-Kanal in dem ihr die Einladung aussprechen wollt und best�tigt anschlie�end.",
	requestInstructions = L"W�hlt in der Dropdownliste den Chat-Kanal in dem ihr um Einladung bitten wollt und best�tigt anschlie�end.",
	channelAdvertise = {
		form = L'Ich bin in der Calling-Gruppe %q. Kommt doch per /calljoin %q dazu, wenn ihr das Addon habt.',
		pat = L'Ich bin in der Calling%-Gruppe "(.*)". Kommt doch per /calljoin "(.*)" dazu, wenn ihr das Addon habt.',
	},
	channelAdvertGotten = '%q hat euch eingeladen der Calling-Gruppe %q beizutreten.',
	join = L"Beitreten",
	ignore = L"Ignorieren",
	channelRequest = {
		form = L'Gibt es schon eine Calling-Gruppe, der ich mich anschlie�en kann?',
		pat = L'Gibt es schon eine Calling-Gruppe, der ich mich anschlie�en (.*)?'
	},
	guild = L"Gilde",
	alliance = L"Allianz",
	group = L"Gruppe",
	warband = L"Kriegstrupp",
	local_ = L"Lokal",
	whereToBroadcast = L"In welchem Chatkanal soll die Callgruppeneinladung erscheinen?",
	
	tutorial = L"SCHNELLEINSTIEG",
	tutDescription = L"Calling bietet Euch eine flexible und  intuitive Nutzung von WARs Assist-Mechanik",
	tutParts = {
		{Title =  L"Setup",
			Text = L"Als einzig n�tigen Setup-Schritt m�sst Ihr die Macros \"Call\" und \"Target\" in eure Fertigkeitsleiste ziehen.\n�ffnet dazu das Untermenu \"Makros\" und zieht dann per Drag&Drop die Makros an die gew�nschten Positionen."},
		{Title =  L"Calling-Gruppen",
			Text = L"Bevor Ihr jedoch euren Schaden fokussieren k�nnt, muss eine Calling-Gruppe gebildet werden.\nKlickt auf das Calling-Symbol links neben der Minimap um eine solche Gruppe zu erstellen oder verwalten."},
		{Title =  L"Szenarien",
			Text = L"Wenn Ihr einem Szenario beitretet, wechselt Ihr automatisch in eine Calling-Gruppe mit allen anderen Spielern in eurem Szenario.\nNach dem Szenario werdet ihr wieder in eure vorherige Calling-Gruppe versetzt."},
		{Title =  L"Call",
			Text = L"Durch Nutzung des Call-Makros k�nnt ihr Unterst�tzung gegen euer aktuelles Ziel anfordern.\nAlle Mitglieder eurer Calling-Gruppe erhalten dann eine Benachrichtigung auf ihrem Bildschirm."},
		{Title =  L"Target",
			Text = L"Wenn ihr eine Call Benachrichtigung erhaltet, k�nnt ihr das \"Target\" Makro benutzen um das  Ziel des Callers zu �bernehmen.\nWenn der Caller in der Zwischenzeit das Ziel gewechselt hat, �bernehmt ihr sein neues Ziel."},
		{Title = L"Anpassung",
			Text = L"Um die Call- und Kill- Listen, das Calling Symbol oder die Call Benachrichtigung zu verschieben, w�hlt \"Oberfl�che anpassen\" im Hauptmenu und klickt dann auf \"Oberfl�che gestalten\"."},
		{Title =  L"Anzeige",
			Text = L"Die Calls- und Kills- Listen k�nnen mit Hilfe des  \"Anzeige\" Untermenus versteckt werden.\nDas Calling Setup k�nnt ihr per Rechtsklick auf das Calling-Symbol ein- und ausblenden."}
	},
}	
