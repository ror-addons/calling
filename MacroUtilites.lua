-- Copyright (c) 2009, Kristian Bergmann
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
--   * Redistributions of source code must retain the above copyright notice,
--     this list of conditions and the following disclaimer.
--   * Redistributions in binary form must reproduce the above copyright
--     notice, this list of conditions and the following disclaimer in the
--     documentation and/or other materials provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

--Returns the id of the macro which has the given description text, if found
function GetMacroId(text)
	local macros = DataUtils.GetMacros()
	local foundSlot = nil
	for k, macro in pairs(macros) do
		if macro.text == text then foundSlot = k end
	end

	return foundSlot
end

--Ensures that a macro with the given data exists and returns it's id
function CreateMacro(name, text, iconID)
	local macros = DataUtils.GetMacros()
	local freeSlot = -1
	local foundSlot = -1
	
	--find the position of the given macro text and a free macro slot
	for k, macro in pairs(macros) do
		if macro.text == text then foundSlot = k end
		if freeSlot == -1 and macro.text == L"" then
			freeSlot = k
		end
	end

	local id = -1
	--set id of either the found slot with the existing macro or the free slot
	if foundSlot ~= -1 then
		id = foundSlot
	elseif freeSlot ~= -1 then
		id = freeSlot
	else
		d(L"Could not create macro: "..name)
		return nil
	end

	--set data of the found or free macro to the desired values
	local macro = macros[id]
	macro.name = name
	macro.text = text
	macro.iconNum = iconID
	
	--tell WAR to update the internal macro system
	EA_Window_Macro.UpdateMacros()
	
	return id
end

--Returns the macro at the given id
function GetMacro(id)
	if id == -1 then
		return nil
	end
	
	local macros = DataUtils.GetMacros()
	return macros[id]
end
