-- Copyright (c) 2009, Kristian Bergmann
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
--   * Redistributions of source code must retain the above copyright notice,
--     this list of conditions and the following disclaimer.
--   * Redistributions in binary form must reproduce the above copyright
--     notice, this list of conditions and the following disclaimer in the
--     documentation and/or other materials provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

--offers event id's and mechanisms to fire events and register and unregister event handlers
CallingEvents = {
	EVENT_NEW_PRIMARY_CALL = 1,
	
	EventHandlers = {}
}

function CallingEvents:RegisterEventHandler(eventID, handlerFunction)
	if self.EventHandlers[eventID] == nil then
		self.EventHandlers[eventID] = LinkedList.Create()
	end
	
	if not self.EventHandlers[eventID]:Contains(handlerFunction) then
		self.EventHandlers[eventID]:PushBack(handlerFunction)
	end
end

function CallingEvents:UnregisterEventHandler(eventID, handlerFunction)
	if self.EventHandlers[eventID] ~= nil then
		self.EventHandlers[eventID]:Remove(handlerFunction)
	end
end

function CallingEvents:FireEvent(eventID)
	if self.EventHandlers[eventID] ~= nil then
		local iterator = self.EventHandlers[eventID].First
		while iterator ~= nil do
			iterator.Data()
			iterator = iterator.Next
		end
	end
end