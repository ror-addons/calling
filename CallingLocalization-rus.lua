-- Copyright (c) 2009, Kristian Bergmann
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
--   * Redistributions of source code must retain the above copyright notice,
--     this list of conditions and the following disclaimer.
--   * Redistributions in binary form must reproduce the above copyright
--     notice, this list of conditions and the following disclaimer in the
--     documentation and/or other materials provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS L"AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

--Definition of the english localization of the Calling addon.
--In order to define the localization for a  different language, copy this file
--and change the language identifier in the next line as well as all strings.
--Placeholders (in parantheses) need to stay in the given amount and order.
Calling.localization[SystemData.Settings.Language.RUSSIAN] = {
	--used for parsing damage reports on enemies from combat log
	hitPhrases = {
		{L"^([^%(]-) наносит цели %((.-)%) (%d+) ед%..-урона%.", function (s,e,r) return e end },
	},
	
	--used for parsing pet's damge reports from combat log
	petPhrases = {
		{L"^(.-) питомец%) наносит цели %((.-)%) (%d+) ед%..-урона%.", function (s,e,r) return e end },
	},

	--used to parse incoming damage from combat log
	dmgPhrases = {
		{L"^(.-) %((.-)%) наносит вам (%d+) ед%..-урона%.", function (s, e, r) return e end},
	},
	
	--used to parse kill messages from combat log
	killPattern = L"^Игрок (.*) .* игроком.-",
	
	addonname=L"ГОПОКЛИЧ",
	notificationWindowCaption = L"Гопоклич - Объявы",
	notificationWindowDescription = L"Показывает последнюю цель.",
	listWindowCaption = L"Гопоклич - Список кличей",
	listWindowDescription = L"Показывает список последних кличей.",
	killsWindowCaption = L"Гопоклич - Список убийств по кличу",
	killsWindowDescription = L"Показывает список последних убийств по кличу.",
	callingIconCaption = L"Гопоклич - иконка",
	callingIconDescription = L"Кнопка для управления гопокличем",
	callInited = L"Гопоклич инициализирован",
	noLibSlashFeatures = L"Если вы хотите использовать слеш-команды \"Гопоклича\", пожалуйста установите плагин \"LibSlash\".",
	noLibCombatLogFeatures = L"Если вы хотите чтобы враги убитые в RvR исчезали из Списка кличей автоматически, вам нужно активировать плагин \"LibCombatLog\".",
	callSlashInited = L"Слеш-команды зарегистрированы",
	useCalljoin = L"! Используйте /calljoin чтобы отправлять и получать кличи !",
	callLabel = L" от ",
	chanLeft = L"Покидаю клич-группу: ",
	calljoinUsage = L"использование: /calljoin 'Клич-группа'",
	chanJoined = L"Вступаю в клич-группу: ",
	markEnabled = L"Маркер цели включен",
	markDisabled = L"Маркер цели выключен",
	autoEnabled = L"Авто-ассист включен",
	autoDisabled = L"Авто-ассист выключен",
	dmgText = L" урона",
	
	subMenus = L"Подменю",
	
	macros = L"Макросы",
	macroInstructions = L"Перетащите эти два макроса на вашу панель умений чтобы использовать ГопоКлич.",
	
	keybindings = L"Привязка клавиш",
	boundKey = L"Привязан",
	bindSelectInstructions = L"Чтобы выбрать горячие клавиши для макроса, выберите его на вашей панели умений с помощью щелчка правой кнопкой мыши",
	bindClickInstructions = L"Щелкните по кнопке ниже чтобы выбрать горячую клавишу.",
	bindInstructions = L"Щелкните желаемый ярлык или щелкните опять на кнопке для сброса.",
	noSlotSelected = L"Ячейка умения не выбрана.",
	selectSlot = L"Выберете ячейку",
	selectSlotToProceed = L"Выберите ячейку панели умений чтобы продолжить.",
	selectedSlot = L"Выберите ячейку умения: ",
	inBar = L" в панели ",
	
	priorities = L"Приоритеты",
	prioInstructions = L"Эти приоритеты определяют важность различных кличей.\nЕсли клич поступает с высоким приоритетом он оказывается выше в списке кличей.\nЕсли приоритет нового клича выше чем текущего, новый клич становится текущим.",
	prio1 = L"Высший приоритет",
	prio2 = L"2-й приоритет",
	prio3 = L"3-й приоритет",
	type = L"Тип",
	selection = L"Выбор",

	display = L"Вывод",
	showGroupIcons = L"Показывать иконки членов группы (GroupIcons)",
	showCallList = L"Показывать список кличей",
	showKillList = L"Показывать список убийств",
	showCallerClass = L"Показывать класс коллера",
	showTargetClass = L"Показывать класс цели",
	notificationCareerColor = L"???",
	language = L"Язык",
	languageName = L"Русский",
	activeLanguage = L"Язык клиента игры",

	targeting = L"Цели",
	autoAssist = L"Авто-ассист \n(также управляется средним кликом на иконку)",
	showTargetMarker = L"Показать маркер цели (производительность)",
	evaluateRvrKills = L"Удалять клич после смерти цели игрока (производительность)",
	removeNpc = L"Удалять клич после смерти цели NPC",
	
	groupMgmt = L"Управление группой",	
	inviteDesc = L"Диалог при приглашение в клич-группу:",
	showGroupInvites = L"Показывать",
	showInvitesWhenGrouped = L"Скрывать, если я уже в клич-группе",
	inviteOnRequestDesc = L"Послать приглашение по запросу:",
	reactOnInvitationRequestsGnA = L"Запрос из гильдии и альянса",
	reactOnInvitationRequestsPnW = L"Запрос из группы и ВБ",
	reactOnInvitationRequestsL = L"Запрос из общего чата",		

	age = L"Сек",
	caller = L"Коллер",
	target = L"Цель",
	lived = L"Сек",
	killedTarget = L"Убит",
	clickToAssist = L"Щелкните для ассиста ",
	versus = L" на ",
	rightClickToRemove = L"Удалить запись из списка.",
	updating = L"--обновляю--",
	userList = L"Список коллеров",
	
	priorityTypes = {
		[Calling.Priority.CALLER] = L"Коллер",
		[Calling.Priority.CALLER_TYPE] = L"Архетип коллера",
		[Calling.Priority.CALLER_CAREER] = L"Класс коллера",
		[Calling.Priority.TARGET_TYPE] = L"Архетип цели",
		[Calling.Priority.TARGET_CAREER] = L"Класс цели",
		[Calling.Priority.CALLER_GROUP] = L"???",
		[Calling.Priority.NONE] = L"-Нет-"
	},
	
	characterTypes = {
		[Calling.Archetypes.MELEE_DMG] = L"Мили-дамаг",
		[Calling.Archetypes.RANGED_DMG] = L"Ренж-дамаг",
		[Calling.Archetypes.HEALER] = L"Хилер",
		[Calling.Archetypes.TANK] = L"Танк"
	},

	groupTypes = {
		[Calling.Grouptypes.PARTY] = L"???",
		[Calling.Grouptypes.WARBAND] = L"???",
		[Calling.Grouptypes.SCENARIO_GROUP] = L"???"
	},

	choosePrioType = L"Выберите тип приоритета",
	
	version06Warning = L"ГОПОКЛИЧ: Scenario-Callgroups changed! Remind your comrades-in-arms to install Calling v0.6+ in order to coordinate!",
	timePart1 = L" (",
	timePart2 = L" назад)",
	
	braggingTime = L" был убит ",
	braggingCaller = L" после получения клича от ",
	braggingAssisters = L", помощь от ",
	braggingKilled = L" ассистентов.",
	clickToInsert = L"Щелкните, чтобы вставить этот текст в строку ввода чата: ",
	groupManagement = L"ЛевКлик: Вступление, выход и управление клич-группой",
	settingsShownToggle = L"ПравКлик: Показать/скрыть окно настройки Гопоклича",
	groupFounder = L"Главный коллер: ",

 	noChannelOptions = L'"Создать" создает клич-группу %q. Используя "Запрос" вы можете запросить вступление в открытые клич-группы.',
	cancel = L"Отмена",
	request = L"Запрос",
	create = L"Создать",
	inChannelOptions = L'Используя "Пригласить" вы можете пригласить своих товарищей в свою клич-группу. "Покинуть" - покинуть вашу клич-группу.',
	invite = L"Пригласить",
	leave = L"Покинуть",
	inviteInstructions = L"Выберите чат-канал чтобы послать в него приглашение из выпадающего списка и подтвердите.",
	requestInstructions = L"Выберите чат-канал чтобы послать в него запрос на приглащение из выпадающего списка и подтвердите.",
	channelAdvertise = {
		form = L"Я в клич-группе %q. Присоединяйтесь ко мне с помощью /calljoin %q, если у вас есть аддон.",
		pat = L"^Я в клич%-группе \"(.+)\". Присоединяйтесь ко мне.-"
	},
	channelAdvertGotten = L'%q пригласил вас в клич-группу %q.',
	join = L"Вступить",
	ignore = L"Игнорировать",	
	channelRequest = {
		form = L'Есть кто в открытой клич-группе?',
		pat = L'^Есть (.*) в открытой клич%-группе?'
	},
	guild = L"Гильдия",
	alliance = L"Альянс",
	group = L"Группа",
	warband = L"Варбанда",
	local_ = L"Общий",
	whereToBroadcast = L"В какой чат-канал послать приглашение в клич-группу?",
	
	tutorial = L"УЧЕБНИК",
	tutDescription = L"Гопоклич предлагает гибкое использование ассист-механики игры и добавляет к этому полезные фичи.",
	tutParts = {
		{Title =  L"Настройка",
			Text = L"Единственно что вам нужно настроить - это переместить макросы \"Call\" и \"Target\" на вашу панель умений.\nВыберите подменю \"Макросы\" и перетащите два макроса на вашу панель умений."},
		{Title =  L"Вступление",
			Text = L"Прежде чем вы сможете фокусировать дамаг с помощью клича, вам нужно собрать клич-группу.\nЩелкните на значок ГопоКлича слева от карты чтобы сформировать и управлять клич-группой."},
		{Title =  L"Клич",
			Text = L"Вы можете кликнуть свою цель с помощью макроса \"Call\".\nЭто вызовет объявления на экранах членов вашей клич-группы и автоматически сменит их цель на вашу если у них включена опция Авто-ассистирования."},
		{Title =  L"Авто-ассист",
			Text = L"Еслои опция Авто-ассиста включена, то ваша цель автоматически сменится на цель коллера, когда тот использует макрос \"Call\".\nЕсли Авто-ассист выключен, вам нужно будет самостоятельно использовать макрос \"Assist\"."},
		{Title =  L"Цель",
			Text = L"Когда вы заметили что коллер кликнул цель, вы можете использовать макрос \"Assist\" чтобы выбрать текущую цель коллера.\nЕсли коллер кликнул новую цель вместо текущей, вы будете ассистировать ему с новой целью."},
		{Title = L"Кастомизация",
			Text = L"Если вы хотите переместить или изменить размер списков клича или убийств, кнопки ГопоКлича или Обьяв -  выберите \"Настройка интерфеса\" из основного меню и затем кликните на \"Редактор интерфейса\"."},
		{Title =  L"Видимость",
			Text = L"Вы можете спрятать списки кличей и убийств используя подменю \"Вывод\".\nПравый клик на кнопку Гопоклича управляет видимостью этого окна настройки."}
	},
}
