-- Copyright (c) 2009, Kristian Bergmann
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
--   * Redistributions of source code must retain the above copyright notice,
--     this list of conditions and the following disclaimer.
--   * Redistributions in binary form must reproduce the above copyright
--     notice, this list of conditions and the following disclaimer in the
--     documentation and/or other materials provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

CallingTests = {
	CallingChannel = -1,
	TestsRunning = false,
	TestTimeout = 10,
	TimeBetweenTests = 0,
	
	PreTestCallingGroup = L"",
	TestFails = L"",
	
	TimeToStageStart = 0,
	TimeSinceStageStart = 0,
	CurrentTest = nil,
	TestsToRun = LinkedList.Create(),
	
	--test constants
	TestChan = L"CallingTestChan",
	CallerName = L"MyCaller",
	TargetName = L"MyTarget"
}

-- ------------------------------------------
TestCallgroupJoin = {
	Init = function ()
		Calling.Print(L"test: callgroup join")
		Calling.RequestJoin(CallingTests.TestChan)
	end, 

	TestCondition = function ()
		if Calling.Chat ~= -1 then
			local chanOk =  ChatSettings.Channels[Calling.Chat].labelText:find(CallingTests.TestChan) ~= nil
			local cachedOk = CallingTests.TestChan:find(Calling.Channel) ~= nil
			return chanOk and cachedOk
		else
			return false
		end
	end
}
-- ------------------------------------------
TestCallerJoin =
 {
	Init = function ()
		Calling.Print(L"test: caller join")
		CallingTests:FakeChatMessage(Calling.Chat, CallingTests.CallerName, L"+13+")
	end,

	TestCondition = function ()
			for i = 1, #Calling.UserList do
				if Calling.UserList[i]:find(CallingTests.CallerName) ~= nil then
					return true
				end
			end
			return false
			--TODO MyCaller wird scheinbar durch eigenen namen ersetzt? nicht mehr!
	end
}
-- ------------------------------------------
TestCall =
 {
	Init = function ()
		Calling.Print(L"test: call")
		CallingTests:FakeChatMessage(Calling.Chat, CallingTests.CallerName,
			Calling.callText[1]..CallingTests.TargetName..L" (P)[24]-12-")
	end,

	TestCondition = function ()
		for i = 1, CallsList.MAX_SAVED_CALLS do
			local call = Calling.GetCallFromList(i)
			if call ~= nil and
				call.TargetCareerID == 24 and
				not call.TargetIsMonster and
				call.Caller:find(CallingTests.CallerName) ~= nil and
				call.Target:find(CallingTests.TargetName) ~= nil and
				call.CallerCareerID == 12 and
				call.TargetIsPlayer
			then
				return true
			end					
		end
		return false
	end
}
-- ------------------------------------------
TestCallerLeave =
 {
	Init = function ()
		Calling.Print(L"test: caller leave")
		CallingTests:FakeChatMessage(Calling.Chat, CallingTests.CallerName, L"--")
		--TODO eventuell test f�r unangemeldetes verlassen?
	end,

	TestCondition = function ()
		for i = 1, #Calling.UserList do
			if Calling.UserList[i]:find(CallingTests.CallerName) ~= nil then
				return false
			end
		end
		return true
	end
}
-- ------------------------------------------
TestCallgroupLeave =
 {
	Init = function ()
		Calling.Print(L"test: callgroup leave")
		Calling.QuitChannel()
	end,

	TestCondition = function ()
		local callerInList = false
		for i = 1, #Calling.UserList do
			if Calling.UserList[i]:find(CallingTests.CallerName) ~= nil then
				callerInList = true
			end
		end
		return not callerInList
	end
}
-- ------------------------------------------
Test =
 {
	Init = function ()
	end,

	TestCondition = function ()
	end
}
-- ------------------------------------------

function CallingTests:Run() 
	self.TestsRunning = true
	self.PreTestCallingGroup = Calling.Channel

	self.CurrentTestStage = 0
	self.TimeSinceStageStart = 0
	
	self.TestsToRun = LinkedList.Create()
	self.TestsToRun:PushBack(TestCallgroupJoin)
	self.TestsToRun:PushBack(TestCallerJoin)
	self.TestsToRun:PushBack(TestCall)
	self.TestsToRun:PushBack(TestCallerLeave)
	self.TestsToRun:PushBack(TestCallgroupLeave)
	
	self:LoadNextTest()
end

function CallingTests.OnUpdate(elapsedTime)
	local self = CallingTests
	if self.TestsRunning and self.CurrentTest ~= nil then
		--wait until test has to start and start if it's time
		if self.TimeToStageStart > 0 then
			self.TimeToStageStart = self.TimeToStageStart - elapsedTime
		end		
		if self.TimeToStageStart <= 0 and self.TimeSinceStageStart == 0 then
			self.CurrentTest.Init()
		end

		--check for successfull test
		if self.CurrentTest.TestCondition() then
			Calling.Print(L"test suceeded")
			self:LoadNextTest()
		elseif self.TimeToStageStart <= 0 then
			self.TimeSinceStageStart = self.TimeSinceStageStart + elapsedTime
		end

		--check for test timeout
		if self.TimeSinceStageStart >= CallingTests.TestTimeout then
			Calling.Print(L"TEST FAILED: timed out")
			self.CurrentTest = nil
		end
	elseif self.TestsRunning then
		--tests finished
		Calling.Print(L"Testing finished")
		self.TestsRunning = false
		if self.PreTestCallingGroup ~= nil then
			Calling.RequestJoin(self.PreTestCallingGroup)
		else
			Calling.QuitChannel()
		end
	end
end

function CallingTests:FakeChatMessage(channel, sender, message)
	local oldType = GameData.ChatData.type
	local oldText = GameData.ChatData.text
	local oldName = GameData.ChatData.name
	
	GameData.ChatData.type = channel
	GameData.ChatData.text = message
	GameData.ChatData.name = sender
	Calling.ExamineChatMessage()
	
	GameData.ChatData.type = oldType
	GameData.ChatData.text = oldText
	GameData.ChatData.name = oldName
end

function CallingTests:LoadNextTest()
	if self.TestsToRun.First ~= nil then
		self.CurrentTest = self.TestsToRun.First.Data
		self.TestsToRun:RemoveElement(self.TestsToRun.First)
	else
		self.CurrentTest = nil
	end
	self.TimeSinceStageStart = 0
	self.TimeToStageStart = CallingTests.TimeBetweenTests
end
