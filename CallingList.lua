-- Copyright (c) 2009, Kristian Bergmann
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
--   * Redistributions of source code must retain the above copyright notice,
--     this list of conditions and the following disclaimer.
--   * Redistributions in binary form must reproduce the above copyright
--     notice, this list of conditions and the following disclaimer in the
--     documentation and/or other materials provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

--structure holding information about the calling list
CallsList = {
	--window for displaying the list
	windowName = "CallingList",
	--calls shown in the list
	entries = { },
	MAX_SAVED_CALLS = 3,
	--id of the marked call (one based)
	marked = nil,
	--id for iterating the list of calls
	currentID = 1,
	--window visible?
	shown = true
}

--structure holding information about the kills list (see CallsList for members)
KillsList = {
	windowName = "KillsList",
	entries = { },
	MAX_SAVED_CALLS = 3,
	marked = nil,
	shown = true
}

--Returns first entry from the calls list
function Calling.GetFirstInList()
	CallsList.currentID = 1
	return Calling.GetCallFromList(CallsList.currentID), CallsList.currentID
end

--returns the successor of the last call returned by Calling.GetFirstInList
--or Calling.GetNextInList. Successor of last entry is the first one.
function Calling.GetNextInList()
	CallsList.currentID = math.mod(CallsList.currentID, #CallsList.entries) + 1
	return Calling.GetCallFromList(CallsList.currentID), CallsList.currentID
end

--returns true iff the given call is the first one in the CallsList
function Calling.IsFirstCall(call)
	return #CallsList.entries > 0 and lastCalls[1] == call
end

--returns the id at which the given call is entered in the CallsList or -1 if
--given call is not contained.
function Calling.GetListPosition(call)
	for i = 1, #CallsList.entries do
		if CallsList.entries[i] == call then 
			return i
		end
	end
	
	return -1
end

--Hides the calls and kills lists, if no calling channel is set.
--Shows them depending on the CallingSettings, otherwise.
function Calling.ManageListsShowing()
	if Calling.Chat >= 0 then
		Calling.SetListShown(true)
	else
		Calling.SetListShown(false)
	end
end

--if show is false, calls and kills lists are hidden otherwise they are shown
--depending on the CallingSettings.
function Calling.SetListShown(show)
	if CallingSettings.CallsListShown then
		WindowSetShowing("CallingList", show)
	else
		WindowSetShowing("CallingList", false)
	end
	
	if CallingSettings.KillsListShown then
		WindowSetShowing("KillsList", show)
	else
		WindowSetShowing("KillsList", false)
	end
	
	Calling.ManageEntriesShowing(CallsList)
	Calling.ManageEntriesShowing(KillsList)
end

--tints the entry with the given index in the given index to the color given by r, g, b
function Calling.TintListEntry(list, index, r, g, b)
	local entry = list.windowName.."_Entry"..index
	WindowSetTintColor(entry, r, g, b)
	LabelSetTextColor(entry.."_CallerText" , r, g, b)
	LabelSetTextColor(entry.."_TargetText" , r, g, b)
end

--unmarks the last marked entry of the given list and marks the entry at index
function Calling.MarkListEntry(list, index)
	--unmark last
	if CallsList.marked ~= nil then
		Calling.TintListEntry(list, list.marked, 255, 255, 255)
		list.marked = nil
	end
	
	--mark new
	if index <= #list.entries then
		list.marked = index
		Calling.TintListEntry(list, list.marked, 255, 128, 128)
	end
end

--gets the call at (one based) index from the CallsList
function Calling.GetCallFromList(index)
	if index <= #CallsList.entries then
		return CallsList.entries[index]
	else
		return nil
	end
end

--sets the kills list entries' time and assister-count strings to the values
--at the time of the kill
function Calling.SetKillsListStrings()
	for i = 1, (#KillsList.entries>CallsList.MAX_SAVED_CALLS and CallsList.MAX_SAVED_CALLS or #KillsList.entries) do
		local call = KillsList.entries[i]
		local entry = "KillsList_Entry"..i
		LabelSetText( entry.."_TimeText", call.TimeString)
		LabelSetText( entry.."_AssistText", call.AssistsString)
	end
end

--removes the i-th entry (one based) from the calls list. if targetKilled is
--true the entry is moved to the kills list.
function Calling.RemoveFromCallsList(i, targetKilled)
	local removed = Calling.RemoveCall(CallsList, i)
	if targetKilled and removed ~= nil then
		Calling.InsertCall(KillsList, removed, 1)
		
		removed.TimeString = Calling.GetAgeInformation(removed)
		if removed.Assists == nil then
			removed.AssistsString = towstring(0)
		else
			removed.AssistsString = towstring(removed.Assists)	
		end
		
		Calling.TrimList(KillsList)
		Calling.SetKillsListStrings()
	end
end

--removes the call issued by the given caller from the calls list and moves it
--to the kills list, if targetKilled is true
function Calling.RemoveCallsFrom(caller, targetKilled)
	local caller = Calling.StripString(caller)
	for i = 1,#CallsList.entries do
		if CallsList.entries[i] ~= nil and
			CallsList.entries[i].Caller == caller
		then
			Calling.RemoveFromCallsList(i, targetKilled)
			i = i - 1
		end
	end
end

--removes all calls on the given target from the calls list. if targetKilled is
--true, the calls are moved to the kills list.
function Calling.RemoveCallsOn(target, targetKilled)
	local target = Calling.StripString(target)
	for i = 1,#CallsList.entries do
		if CallsList.entries[i] ~= nil and 
			CallsList.entries[i].Target == target
		then
			Calling.RemoveFromCallsList(i, targetKilled)
			i = i - 1
		end
	end	
end

--moves all calls on the player identified in the given kill message from the
--calls list to the kills list
function Calling.RemoveFromKillMsg(msg)
	local killed = msg:match(Calling.GetLocalized("killPattern"))
	if killed == nil then
		d(L"ERROR: could not parse kill message")
	else
--d(L"removing "..killed..L" from list through kill")
		Calling.RemoveCallsOn(killed, true)
	end
end

--adds the given call to the calls list. if no priorities are set, the new call
--overrides the last calls. otherwise it will be sorted into the calls list
--behind the last (highest index) existing call with higher priority.
function Calling.AddCall(call)
	Calling.RemoveCallsFrom( call.Caller )
	
	local callPrio = Calling.GetCallPriority(call)
--Calling.d("prio: ", callPrio)
	local insertAt = 1
	for i = 1,#CallsList.entries do
		local lastPrio = Calling.GetCallPriority(CallsList.entries[i])
		if lastPrio < callPrio then
			insertAt = insertAt + 1
			i = #CallsList.entries + 1
		end
	end
--Calling.d("insert at: ", insertAt)

	Calling.InsertCall(CallsList, call, insertAt)
	Calling.TrimList(CallsList)
	call.IssuedTime = Calling.relativeTime
end

--returns (time_string, min, sec) for the given call based on the current time
function Calling.GetAgeInformation(call)
	local seconds = math.mod(Calling.relativeTime - call.IssuedTime, 60)
	local minutes = (Calling.relativeTime - call.IssuedTime) / 60
	local wstr
	
	if seconds < 10 then
		wstr = wstring.format(L"%i:0%i", minutes, seconds)
	else
		wstr = wstring.format(L"%i:%i", minutes, seconds)
	end
	
	return wstr, minutes, seconds
end

--shows entries of the given list if they contain data and sets their texts
function Calling.ManageEntriesShowing(list)
	local entryString = list.windowName.."_Entry"
	for i = 1, list.MAX_SAVED_CALLS do
		local show = list.entries[i] ~= nil
		WindowSetShowing(entryString..i, show)
		if show then
			Calling.SetListLine(list, i, list.entries[i])
		end		
	end
end

--inserts the given call into the given list at the given position and updates
--the lists display
function Calling.InsertCall(list, call, position)
	local i = #list.entries
	while i >= position do
		local newID = i + 1
		list.entries[newID] = list.entries[i]
		Calling.SetListLine(list, newID, list.entries[newID])
		i = i - 1
	end
	list.entries[position] = call
	Calling.SetListLine(list, position, call)
	if list.marked ~= nil and list.marked >= position then
		Calling.MarkListEntry(list, list.marked+1)
	end
	Calling.ManageEntriesShowing(list)
end

--removes the call at the given position from the given list and updates
--the lists display. returns the removed call.
function Calling.RemoveCall(list, position)
--d("remove call "..list.windowName.. position)
	local removed = list.entries[position]
	
	--move all following entries to fill the gap
	for i = position + 1, #list.entries do
		list.entries[i - 1] = list.entries[i]
	end
	Calling.ClearListLine(list, #list.entries)
	list.entries[#list.entries] = nil

	--update marked entry if necessary
	if list.marked ~= nil then
		if list.marked > position then
			Calling.MarkListEntry(list, list.marked - 1)
		elseif list.marked == position then
			Calling.MarkListEntry(list, list.MAX_SAVED_CALLS + 1)
		end
	end
	
	Calling.ManageEntriesShowing(list)
	
	return removed
end

--removes all calls from the given list which are more than the lists capacity
--allows.
function Calling.TrimList(list)
	while #list.entries > list.MAX_SAVED_CALLS do
		list.entries[#list.entries] = nil
	end
	Calling.ManageEntriesShowing(list)
end

--clears all text from the given list's entry at the given index.
function Calling.ClearListLine(list, index)
--d("clear line "..list.windowName..index)
	if index <= CallsList.MAX_SAVED_CALLS then
		local entry = list.windowName.."_Entry"..index
		LabelSetText( entry.."_TimeText", L"")
		LabelSetText( entry.."_CallerText", L"")
		LabelSetText( entry.."_TargetText", L"")
		LabelSetText( entry.."_AssistText", L"")
		WindowSetShowing( entry.."_CallerIcon", false )
		WindowSetShowing( entry.."_TargetIcon", false )		
	end

end

--displays the given call's data in the given list's entry at the given line
function Calling.SetListLine(list, listLine, call)
if listLine<=CallsList.MAX_SAVED_CALLS then
	local entry = list.windowName.."_Entry"..listLine
	if call ~= nil then
		--display duration of the call
		if call.TimeString ~= nil then
			LabelSetText( entry.."_TimeText", call.TimeString)
		elseif list == CallsList then
			LabelSetText( entry.."_TimeText", Calling.GetLocalized("age"))
		elseif list== KillsList then
			LabelSetText( entry.."_TimeText", Calling.GetLocalized("lived"))
		end
		
		--diplay target and caller names
		LabelSetText( entry.."_CallerText", towstring(call.Caller))
		LabelSetText( entry.."_TargetText", towstring(call.Target))

		--display number of assisters on the call
		if call.AssistsString ~= nil then
			LabelSetText( entry.."_AssistText", call.AssistsString)
		elseif list == CallsList then
			LabelSetText( entry.."_AssistText", L"x/"..#Calling.UserList)
		elseif list== KillsList then
			LabelSetText( entry.."_AssistText", L"x/?")
		end

		--display the caller career's icon
		if call.CallerCareerID ~= nil then
			WindowSetShowing( entry.."_CallerIcon", true )
			local cTex, x, y = GetIconData( Icons.GetCareerIconIDFromCareerLine( call.CallerCareerID ) )
			DynamicImageSetTexture( entry.."_CallerIcon", cTex, x, y )
		else
			WindowSetShowing( entry.."_CallerIcon", false )
		end

		--display the target career's icon
		if call.TargetCareerID ~= nil then
			WindowSetShowing( entry.."_TargetIcon", true )
			local tTex, x, y = GetIconData( Icons.GetCareerIconIDFromCareerLine( call.TargetCareerID ) )
			DynamicImageSetTexture( entry.."_TargetIcon", tTex, x, y )
		else
			WindowSetShowing( entry.."_TargetIcon", false )
		end
	else
		Calling.ClearListLine(list, listLine);
	end
end
end

--removes calls which are older than 2 minutes and updates all information
--shown about the calls to match the current situation.
function Calling.ManageList()
	for i = 1, (#CallsList.entries>CallsList.MAX_SAVED_CALLS and CallsList.MAX_SAVED_CALLS or #CallsList.entries) do
		local call = CallsList.entries[i]
		local text, minutes, seconds = Calling.GetAgeInformation(call)
		
		if minutes > 2 then
			Calling.RemoveCall(CallsList, i)
			i = i - 1
		else
			local entry = "CallingList_Entry"..i
			LabelSetText( entry.."_TimeText", text)
			if call.Assists == nil then
				LabelSetText( entry.."_AssistText", towstring(0))
			else
				LabelSetText( entry.."_AssistText", towstring(call.Assists))		
			end
		end
	end
end

-- OnClick events

--assists on the call at the given index of the calls list
function Calling.AssistSaved(index)
	if #CallsList.entries >= index then
		Calling.AssistOnCall(CallsList.entries[index], index)
	else
		d(L"no saved call with index "..index)
	end
end

--returns a bragging text containing all information about the death of the
--given call's target.
function Calling.GetBraggingText(call)
	return L"Calling: "..call.Target..
		Calling.GetLocalized("braggingTime")..call.TimeString..
		Calling.GetLocalized("braggingCaller")..call.Caller..
		Calling.GetLocalized("braggingAssisters")..call.AssistsString..
		Calling.GetLocalized("braggingKilled")
end

--manages clicks on both the calls and the kills list.
--calls list: assists on the clicked call
--kills list: inserts bragging text into the chat text entry field
function Calling.OnListEntryClick()
	local activeWindow = SystemData.ActiveWindow.name
	local callsEntryID = activeWindow:match("CallingList_Entry(.*)")
	local killsEntryID = activeWindow:match("KillsList_Entry(.*)")
	if callsEntryID ~= nil then
		Calling.AssistSaved(tonumber(callsEntryID))
	elseif killsEntryID ~= nil then
		local call = KillsList.entries[tonumber(killsEntryID)]
		local braggingText = Calling.GetBraggingText(call)
		EA_ChatWindow.InsertText(braggingText)
	end
end

--removes the clicked entry from the calls or kills list
function Calling.OnListEntryRightClick()
	local activeWindow = SystemData.ActiveWindow.name
	local callsEntryID = activeWindow:match("CallingList_Entry(.*)")
	local killsEntryID = activeWindow:match("KillsList_Entry(.*)")
	if callsEntryID ~= nil then
		Calling.RemoveCall(CallsList, callsEntryID);
	elseif killsEntryID ~= nil then
		Calling.RemoveCall(KillsList, killsEntryID);
	end	
end

--shows a tooltip describing the onclick action for the givn calls list entry
function CallsList.MouseOver(index)
	if CallsList.entries[index] ~= nil then
		Tooltips.CreateTextOnlyTooltip(SystemData.ActiveWindow.name)
			
		Tooltips.SetTooltipColorDef(1, 1, Tooltips.COLOR_HEADING)
		Tooltips.SetTooltipText(1, 1, L"LM: "..
			Calling.GetLocalized("clickToAssist")..
			towstring(CallsList.entries[index].Caller)..
			Calling.GetLocalized("versus")..
			towstring(CallsList.entries[index].Target)..L".")
		Tooltips.SetTooltipText(2, 1, L"RM: "..
			Calling.GetLocalized("rightClickToRemove"))
		Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_BOTTOMLEFT)
		Tooltips.Finalize()
	end
end

--shows a tooltip describing the onclick action for the givn kills list entry
function KillsList.MouseOver(index)
	local call = KillsList.entries[index]
	if call ~= nil then
		local braggingText = Calling.GetBraggingText(call)
		
		Tooltips.CreateTextOnlyTooltip(SystemData.ActiveWindow.name)
		Tooltips.SetTooltipColorDef(1, 1, Tooltips.COLOR_HEADING)
		Tooltips.SetTooltipText(1, 1, L"LM: "..
			Calling.GetLocalized("clickToInsert")..
			L'"'..braggingText..L'"')
		Tooltips.SetTooltipText(2, 1, L"RM: "..
			Calling.GetLocalized("rightClickToRemove"))
		Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_BOTTOMLEFT)
		Tooltips.Finalize()
	end
end

--shows a tooltip describing the onclick action for the entries of the 
--calls and kills lists
function Calling.OnListEntryMouseOver()
	local activeWindow = SystemData.ActiveWindow.name
	local callsEntryID = activeWindow:match("CallingList_Entry(.*)")
	local killsEntryID = activeWindow:match("KillsList_Entry(.*)")
	if callsEntryID ~= nil then
		CallsList.MouseOver(tonumber(callsEntryID))
	elseif killsEntryID ~= nil then
		KillsList.MouseOver(tonumber(killsEntryID))
	end
end

local lastUserListUpdate = -5000

--shows a list containing the users in the current calling channel on hovering
--above the calls list header
function Calling.OnListHeaderMouseOver()
	if SystemData.ActiveWindow.name:find("CallingList") then
		lastUserListUpdate = Calling.relativeTime
		Calling.UpdateUserListTooltip()
	end
end

--generates the tooltip showing the current user list of the calling channel
function Calling.UpdateUserListTooltip()
	Tooltips.CreateTextOnlyTooltip("CallingList_Entry0")
	Tooltips.SetTooltipColorDef(1, 1, Tooltips.COLOR_HEADING)
	Tooltips.SetTooltipText(1, 1, Calling.GetLocalized("userList"))
	Tooltips.SetTooltipText(2, 1, Calling.GetLocalized("groupFounder")..Calling.Channel)
	if Calling.IsUserListUpdating() then
		Tooltips.SetTooltipText(3, 1, Calling.GetLocalized("updating"))
	else
		Tooltips.SetTooltipText(3, 1, L" ")
	end
	for i = 1, #Calling.UserList do
		Tooltips.SetTooltipText(3 + i, 1, Calling.UserList[i])
	end
	Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_BOTTOMLEFT)
	Tooltips.Finalize()
	
	Calling.SetListLine(CallsList, 0, CallingInit.CallsHeader)
end
