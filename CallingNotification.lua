-- Copyright (c) 2009, Kristian Bergmann
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
--   * Redistributions of source code must retain the above copyright notice,
--     this list of conditions and the following disclaimer.
--   * Redistributions in binary form must reproduce the above copyright
--     notice, this list of conditions and the following disclaimer in the
--     documentation and/or other materials provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

--current text shown in the call notification line
local labelText = L""

--scale of the targets career icon
local iconScale = 1.5
--alpha blending strength of the targets career icon
local iconAlpha = 0.8

--scale of the callers career icon
local callerIconScale = 0.7
--alpha blending strength of the callers career icon
local callerIconAlpha = 0.8

local notificationTextScale = 1

--------------------------
-- HIGHLIGHTING SECTION --
--------------------------

--is the call notification highlighted currently?
local highlightShown = false
--has the program requested highlightin of the call notification?
local highlightWanted = false

--removes the highlighting from whatever window is currently highlighted
function Calling.StopHighlight()
	if( WindowGetShowing( HelpTips.FOCUS_WINDOW_NAME ) ) then
		WindowStopAlphaAnimation( HelpTips.FOCUS_WINDOW_NAME )
		WindowSetShowing( HelpTips.FOCUS_WINDOW_NAME, false )
	end
	highlightShown = false
end

--highlights the given window. stops possible highlighting of any other window.
function Calling.HighlightWindow( window )
	Calling.StopHighlight()
	HelpTips.SetFocusOnWindow( window )
	highlightShown = true
end

--sets the call notifivation's text and higlights it.
function Calling.ShowBlinkingText(text)
	Calling.sinceLastCall = 0
	labelText = text
	highlightWanted = true
end

--sets the target career icon to represent the given career or hides it,
--depending on CallingSettings.TargetCareerShown
function Calling.ShowTargetIcon(careerId)
	if careerId ~= nil and CallingSettings.TargetCareerShown then
		local texture, x, y = GetIconData( Icons.GetCareerIconIDFromCareerLine( careerId ) )
		DynamicImageSetTexture( "CallWindowIcon", texture, x, y )
		WindowSetScale( "CallWindowIcon", iconScale * WindowGetScale("CallWindow"))
		WindowSetAlpha( "CallWindowIcon", iconAlpha )
	else
		WindowSetAlpha( "CallWindowIcon", 0 )
	end
end

--sets the callers career icon to represent the given career or hides it,
--depending on CallingSettings.CallerCareerShown
function Calling.ShowCallerIcon(careerId)
	if careerId ~= nil and CallingSettings.CallerCareerShown then
		local texture, x, y = GetIconData( Icons.GetCareerIconIDFromCareerLine( careerId ) )
		DynamicImageSetTexture( "CallWindowCallerIcon", texture, x, y )
		WindowSetScale( "CallWindowCallerIcon", iconScale * callerIconScale * WindowGetScale("CallWindow"))
		WindowSetAlpha( "CallWindowCallerIcon", iconAlpha * callerIconAlpha)
	else
		WindowSetAlpha( "CallWindowCallerIcon", 0 )	
	end
end

--Sets all parts of the calling notification to represent the given call and
--highlights if it is the top priority call.
function Calling.ShowNotification(call)
	local listPosition = Calling.GetListPosition(call)
	if listPosition > 0 then
		--notification text is built step by step in order to align icons
		WindowSetDimensions("CallWindowText", 800, 70)
		local text = call.Caller..Calling.GetLocalized("callLabel")..towstring(call.Target)
		
		--set career icons
		Calling.ShowCallerIcon(tonumber(call.CallerCareerID))
		if call.TargetIsMonster  then
			WindowSetAlpha( "CallWindowIcon", 0 )
		elseif call.TargetIsPlayer then
			Calling.ShowTargetIcon(tonumber(call.TargetCareerID))
		end

		--adds the time that passed from issuing the call if it already has been tagged with an issuing time
		if call.IssuedTime ~= nil then
			local timeStr, minutes, seconds = Calling.GetAgeInformation(call)
			if minutes * 60 + seconds > 1 then
				timeString = Calling.GetLocalized("timePart1") .. timeStr .. Calling.GetLocalized("timePart2")
				text = text..timeString
			end
		end

		--shows the call notification and highlights it iff it is the current call
		Calling.ShowBlinkingText(text)
		if listPosition == 1 and not LayoutEditor.isActive then
--			Calling.ShowBlinkingText(L" ! "..text..L" ! ")
			PlaySound( Calling.SoundNum)
		else
--			local prioText = L" ("..listPosition..L") "
--			Calling.ShowBlinkingText(prioText..text..prioText)
			Calling.StopHighlight()
		end
		
		CallingEvents:FireEvent(CallingEvents.EVENT_NEW_PRIMARY_CALL)
	end
end

local testCallShown = false
local sinceTestCall = 0

function Calling.ResizeOrMoveNotification(elapsedTime)
	if LayoutEditor.isActive then
		Calling.RemoveCallsFrom(L"SomeCaller", false)
		local fakeCall = {Caller = L"SomeCaller",
				Target = L"SomeTarget",
				TargetIsPlayer = true,
				TargetIsMonster = false,
				TargetCareerID = 24,
				CallerCareerID = 12}

		Calling.AddCall(fakeCall)
		Calling.ShowNotification(fakeCall)
		testCallShown = true
	elseif testCallShown then
		Calling.RemoveCallsFrom(L"SomeCaller", false)
		Calling.StopHighlight()
		labelText = L""
		testCallShown = false
	end
end

--updates the notification window. 
function Calling.ManageNotification(elapsedTime)
	Calling.ResizeOrMoveNotification(elapsedTime)
	
	local cs = CallingSettings
	if labelText ~= L"" and Calling.sinceLastCall < Calling.callStaysFor then
		Calling.sinceLastCall = Calling.sinceLastCall + elapsedTime

		LabelSetText("CallWindowText", labelText)
		
		if Calling.lastCall ~= nil then
			--fade out career icons, starting at half the notification time
			if Calling.sinceLastCall > Calling.callStaysFor / 2 then
				local relativeTime = Calling.sinceLastCall / Calling.callStaysFor
				local relativeFromHalf = (relativeTime - 0.5) * 2
				local alpha = 1 - relativeFromHalf
				if Calling.lastCall.CallerCareerID ~= nil and
					CallingSettings.CallerCareerShown 
				then
					WindowSetAlpha("CallWindowCallerIcon", iconAlpha * alpha)
				end
				if Calling.lastCall.TargetCareerID and 
					CallingSettings.TargetCareerShown 
				then
					WindowSetAlpha("CallWindowIcon", iconAlpha * alpha)
				end
			end

			--set color of the notification text depending on call priority and target type
			if Calling.GetListPosition(Calling.lastCall) ~= 1 then
				LabelSetTextColor("CallWindowText", cs.lowPrioR, cs.lowPrioG, cs.lowPrioB)
			elseif Calling.lastCall.TargetIsMonster then 
				LabelSetTextColor("CallWindowText", cs.mR, cs.mG, cs.mB)
			elseif Calling.lastCall.TargetIsPlayer then
				if Calling.lastCall.TargetCareerID and CallingSettings.NotificationCareerColor then
					LabelSetTextColor("CallWindowText", unpack(Calling.GetCareerLineColor( Calling.lastCall.TargetCareerID )))
				else
					LabelSetTextColor("CallWindowText", cs.pR, cs.pG, cs.pB)	
				end
			end
		else
			LabelSetTextColor("CallWindowText", cs.nR, cs.nG, cs.nB)
		end
		
		--remove highlight after half the notification time
		if Calling.sinceLastCall > 2 then
			highlightWanted = false
		end
		
		--manage highlighting of the notification window
		if highlightWanted ~= highlightShown then
			if highlightWanted then
				local x, y = LabelGetTextDimensions("CallWindowText")
				WindowSetDimensions("CallWindowText", x+8, y)
				Calling.HighlightWindow("CallWindowText")
			else
				Calling.StopHighlight()
			end
		end
	else
		--hide all parts of the notification if there is no new call
		WindowSetAlpha("CallWindowIcon", 0)
		WindowSetAlpha("CallWindowCallerIcon", 0)
		LabelSetText("CallWindowText", towstring(""))
		LabelSetTextColor("CallWindowText", cs.nR, cs.nG, cs.nB)
		WindowSetDimensions("CallWindowText", 800, 70)
		--WindowStartAlphaAnimation("CallWindowText", Window.AnimationType.EASE_OUT, 1, 0, 2, false, 0, 1)
	end
end

--------------------------
-- TEXT COLOR BASED ON CAREER --
--------------------------

local careerArchSettings = {
	[GameData.CareerLine.IRON_BREAKER] 				= "colortype-ironbreaker",
	[GameData.CareerLine.SWORDMASTER] 				= "colortype-swordmaster",
	[GameData.CareerLine.CHOSEN] 					= "colortype-chosen",
	[GameData.CareerLine.BLACK_ORC] 				= "colortype-blackorc",
	[GameData.CareerLine.WITCH_HUNTER] 				= "colortype-witchhunter",
	[GameData.CareerLine.SEER] 						= "colortype-whitelion",
	[GameData.CareerLine.WARRIOR] 					= "colortype-marauder",
	[GameData.CareerLine.ASSASSIN] 					= "colortype-witchelf",
	[GameData.CareerLine.BRIGHT_WIZARD] 				= "colortype-brightwizard",
	[GameData.CareerLine.MAGUS] 					= "colortype-magus",
	[GameData.CareerLine.SORCERER] 					= "colortype-sorcerer",
	[GameData.CareerLine.ENGINEER] 					= "colortype-engineer",
	[GameData.CareerLine.SHADOW_WARRIOR] 			= "colortype-shadowwarrior",
	[GameData.CareerLine.SQUIG_HERDER] 				= "colortype-squigherder",
	[GameData.CareerLine.WARRIOR_PRIEST] 			= "colortype-warriorpriest",
	[GameData.CareerLine.BLOOD_PRIEST] 				= "colortype-discipleofkhaine",
	[GameData.CareerLine.ARCHMAGE] 					= "colortype-archmage",
	[GameData.CareerLine.SHAMAN] 					= "colortype-shaman",
	[GameData.CareerLine.RUNE_PRIEST]			 	= "colortype-runepriest",
	[GameData.CareerLine.ZEALOT] 					= "colortype-zealot",
	[GameData.CareerLine.KNIGHT] 					= "colortype-kotbs",
	[GameData.CareerLine.SHADE] 					= "colortype-blackguard",
	[GameData.CareerLine.CHOPPA] 					= "colortype-choppa",
	[GameData.CareerLine.SLAYER] 					= "colortype-slayer",

	["colortype-ironbreaker"]	= "tank",
	["colortype-swordmaster"]	 = "tank",
	["colortype-chosen"]		= "tank",
	["colortype-blackorc"]		= "tank",
	["colortype-kotbs"]		= "tank",
	["colortype-blackguard"]	= "tank",
	["colortype-witchhunter"]	= "mpdps",
	["colortype-whitelion"]		= "mpdps",
	["colortype-marauder"]		= "mpdps",
	["colortype-witchelf"]		= "mpdps",
	["colortype-choppa"]		= "mpdps",
	["colortype-slayer"]		= "mpdps",
	["colortype-brightwizard"]	= "rmdps",
	["colortype-magus"]		= "rmdps",
	["colortype-sorcerer"]		= "rmdps",
	["colortype-engineer"]		= "rpdps",
	["colortype-shadowwarrior"]	= "rpdps",
	["colortype-squigherder"]	= "rpdps",
	["colortype-warriorpriest"]	= "msupp",
	["colortype-discipleofkhaine"]	= "msupp",
	["colortype-archmage"]		= "rsupp",
	["colortype-shaman"]		= "rsupp",
	["colortype-runepriest"]		= "rsupp",
	["colortype-zealot"]		= "rsupp",

	["colorgroup-tank"]		= { 150, 100, 50 }, 	-- brown for tanks
	["colorgroup-mpdps"]		= { 255, 80, 80 }, 		-- red for melee physical dps
	["colorgroup-rpdps"]		= { 255, 180, 0 }, 		-- orange for ranged physical dps
	["colorgroup-rmdps"]		= { 255, 255, 60 }, 	-- yellow for ranged magical dps
	["colorgroup-msupp"]		= { 180, 120, 255 }, 	-- lavender for melee support
	["colorgroup-rsupp"]		= { 100, 150, 255 }, 	-- bluish-purple for ranged support
}

function Calling.GetCareerLineColor( careerLine )
	local default = {255,255,255}
	local color = {255,255,255}

	if( careerArchSettings[careerLine] == nil ) then return default end

    	local archetype = careerArchSettings[ careerArchSettings[careerLine] ]
        if archetype then
        	--Pure.Debug( "Color pulled from Pure." )
            color = careerArchSettings[ "colorgroup-" .. archetype ] or default
        end

	
	return color
end
