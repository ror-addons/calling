Disclaimer
==========

At first i want to apologize for large parts of the code being relatively untidy.
I did not plan on releasing Calling when i started it and it is my first lua project.
As the feature set grew and became more usefull, i decided to release the addon.

Although i know that the code should be cleaned up massively, i won't do this
in order to ensure keeping the plugin functional.
On the one hand i have not found ready-to-use refactoring tools and on
the other hand i see no way of validating hand-refactored lua code without
testing it.
I developed some test-cases that cover some of the core functions of calling,
but they are not extensive enough to test the plugin for functionality. As i
do not play Warhammer actively at the moment i cannot verify the validity of
refactorings at the moment.

In my eyes the most important flaw in the code is the missing use of different
classes for structuring the components.
Next, and connected to that, is the placement of methods of features where
they don't belong.

I hope that with the help of the source code documentation it is possible to
understand the functionality of the addon, although the doc is not as
standartized and extensive as it probably should be.

Code Structure
==============

Having said this, i will try to give the interested developer a short
overview of the project's code structure:

The plugin consists of several components which are listed below:
 * Core functionality:
   * Issuing calls (Calling.lua)
   * Targeting called targets (Calling.lua)
   * The necessary messaging via in-game-chat (CallingChat.lua)
   * Display of the last called target (CallingNotification.lua)
   * Display of the last 3 called targets and last 3 killed targets (CallingList.lua)
   * Graphical User Interface for managing Calling groups (CalljoinGUI.[lua/xml])
 * Additional features:
   * Management of localized strings (CallingLocalization.lua, CallingLocalization-*.lua)
   * Linking enemy players to their client side id's (CallingTargetID.lua, LibCombatLog.(lua/mod))
   * Display of a marker above the current target player's head (CallingTargetMarker.lua)
 * Configuration:
   * General setup of the plugin's behavior (CallingSetup.lua)
   * Helpers for setting up call and target macros (for action bar) (MacroUtilities.lua)
   * Helpers for using the standard WAR routine for keybinding (CallingKeybinding.lua)
 * GUI:
   * Definition of most GUI elements (Calling.xml)
   * Definition of group management related GUI elements (CalljoinGUI.xml)
* Testing
   * Run test library by entering "/script CallingTests:Run()" into chat (CallingTests.lua)
* Events
   * Events can be fired and handlers for them can be registered (CallingEvents.lua)
   

Central addon functions
=======================

Some possible entry points 
* Plugin initialization (Calling.Initialize() / Calling.lua)

* Join a calling group (Calling.RequestJoin(args) / CallingChat.lua)
* Leave the calling group (Calling.QuitChannel() / CallingChat.lua)

* Call current target (Calling.ExecuteCall() / Calling.lua)
* Assist on a previous call (Calling.AssistOnCall(call, listId) / CAlling.lua)

* Sending of messages via chat (Calling.SendChat(channel, message) / CallingChat.lua)
* Examination of messages arriving from the calling chat channel 
	(Calling.ExamineCallMessage() / CallingChat.lua)	

Contact
=======

If you have questions about the plugin's code or architecture or want to
actively maintain the addon, feel free to contact me via curse or email
(narketia [a] gmx.net).