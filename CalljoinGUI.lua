-- Copyright (c) 2009, Kristian Bergmann
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
--   * Redistributions of source code must retain the above copyright notice,
--     this list of conditions and the following disclaimer.
--   * Redistributions in binary form must reproduce the above copyright
--     notice, this list of conditions and the following disclaimer in the
--     documentation and/or other materials provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

CalljoinGUI = {}

--holds possible channels for an action with entries of the form
--{channel,desc}, where channel is the prefix for the chat channel and desc
--is the longer channel name.
local channelOptions = {}
--true iff user has requested sending a calling group invitation and it has
--not been send
local wantBroadcast = false
--true iff user has requested an invitation request for open groups and it
--has not been send
local wantRequestBroadcast = false

--Shows a dialog offering all options a user has when he is not in a Calling
--group (create own group or request invitation to group)
function CalljoinGUI.NoChannelOptions()
	local name = Calling.playerName
	--Calling.Print(name)
	local msg = Calling.GetLocalized("noChannelOptions"):format(name)
	DialogManager.MakeThreeButtonDialog( msg,
		Calling.GetLocalized("create"), function() Calling.RequestJoin(name) end,
		Calling.GetLocalized("request"), CalljoinGUI.ShowRequestOptions,
		Calling.GetLocalized("cancel"), nil)	
end

--Shows a dialog offering all options a user has when he is in a Calling
--group (invite others to group or leave group)
function CalljoinGUI.InChannelOptions()
	local msg = towstring(Calling.GetLocalized("inChannelOptions"):format(towstring(name)))
	DialogManager.MakeThreeButtonDialog( msg,
		Calling.GetLocalized("invite"), CalljoinGUI.ShowBroadcastingOptions,
		Calling.GetLocalized("leave"), Calling.QuitChannel,
		Calling.GetLocalized("cancel"), nil)	
end

--Shows a window presenting the user all channels he can broadcast an
--invitation to his current calling group to.
function CalljoinGUI.ShowBroadcastingOptions()
	wantBroadcast = true
	wantRequestBroadcast = false

	--set captions and description texts
	LabelSetText("CalljoinGUIWindowTitle", Calling.GetLocalized("invite"))
	LabelSetText("CalljoinGUIWindowInstructions", Calling.GetLocalized("inviteInstructions"))
	LabelSetText("CalljoinGUIWindowChannelLabel", L"Chat")

	--fill dropdown with available channels
	CalljoinGUI.FillChannelOptions()
	ComboBoxClearMenuItems("CalljoinGUIWindowChannelCombo")
	for id, chan in pairs(channelOptions) do
		ComboBoxAddMenuItem("CalljoinGUIWindowChannelCombo", chan.desc)
	end
	ComboBoxSetSelectedMenuItem("CalljoinGUIWindowChannelCombo", 1)
	
	--set button texts
	ButtonSetText("CalljoinGUIWindowExecuteBtn", Calling.GetLocalized("invite"))
	ButtonSetText("CalljoinGUIWindowCancelBtn", Calling.GetLocalized("cancel"))
	
	WindowSetShowing("CalljoinGUIWindow", true)
end

--Shows a window presenting the user all channels he can broadcast a
--request for invitation into a currently existing calling group.
function CalljoinGUI.ShowRequestOptions()
	wantBroadcast = false
	wantRequestBroadcast = true

	--set captions and description texts
	LabelSetText("CalljoinGUIWindowTitle", Calling.GetLocalized("request"))
	LabelSetText("CalljoinGUIWindowInstructions", Calling.GetLocalized("requestInstructions"))
	LabelSetText("CalljoinGUIWindowChannelLabel", L"Chat")
	
	--fill dropdown with available channels
	CalljoinGUI.FillChannelOptions()
	ComboBoxClearMenuItems("CalljoinGUIWindowChannelCombo")
	for id, chan in pairs(channelOptions) do
		ComboBoxAddMenuItem("CalljoinGUIWindowChannelCombo", chan.desc)
	end
	ComboBoxSetSelectedMenuItem("CalljoinGUIWindowChannelCombo", 1)
	
	--set button texts
	ButtonSetText("CalljoinGUIWindowExecuteBtn", Calling.GetLocalized("request"))
	ButtonSetText("CalljoinGUIWindowCancelBtn", Calling.GetLocalized("cancel"))
	
	WindowSetShowing("CalljoinGUIWindow", true)
end

--Sends an invitation or request for invitation to the chat channel selected
--via the dialog shown by CalljoinGUI.ShowRequestOptions or
--CalljoinGUI.ShowBroadcastingOptions.
function CalljoinGUI.OnExecuteButton()
	--determine channel
	local channelId = ComboBoxGetSelectedMenuItem("CalljoinGUIWindowChannelCombo")
	local channel = channelOptions[channelId]
	
	--send invitation to the selected channel
	if wantBroadcast then
		CalljoinGUI.BroadcastChannel(channel.channel)
	elseif wantRequestBroadcast then
		CalljoinGUI.RequestBroadcast(channel.channel)
	end
	
	WindowSetShowing("CalljoinGUIWindow", false)
end

--Hides the channel selection window without taking any action.
function CalljoinGUI.OnCancelButton()
	WindowSetShowing("CalljoinGUIWindow", false)
end

--fills the channelOptions
function CalljoinGUI.FillChannelOptions()
	channelOptions = {}
	
	--add group and/or warband channels, if available
	if CalljoinGUI.IsInGroup() then
		channelOptions[#channelOptions + 1] = { channel=L"/p", desc=Calling.GetLocalized("group") }
		if CalljoinGUI.IsInWarband() then
			channelOptions[#channelOptions + 1] = { channel=L"/wb", desc=Calling.GetLocalized("warband") }
		end
	end

	--add guild and alliance channels, if available
	local guild = GameData.Guild
	if  guild ~= nil and
		guild.m_GuildName ~= nil and guild.m_GuildName ~= L""
	then
		channelOptions[#channelOptions + 1] = { channel=L"/g", desc=Calling.GetLocalized("guild") }
		local alliance = guild.Alliance
		if  alliance ~= nil and
			alliance.Name ~= nil and alliance.Name ~= L""
		then
			channelOptions[#channelOptions + 1] = { channel=L"/a", desc=Calling.GetLocalized("alliance") }
		end
	end
	
	--add local channel
	channelOptions[#channelOptions + 1] = { channel=L"/s", desc=Calling.GetLocalized("local_") }
end

--returns true iff the user is in a group
function CalljoinGUI.IsInGroup()
	local group = GetGroupData()
	for id, val in pairs(group) do
		if val.name ~= nil and val.name ~= L"" then
			return true
		end
	end
	return false
end

--returns true iff the user is in a warband
function CalljoinGUI.IsInWarband()
	return IsWarBandActive()
end

--sends an invitation to the user's current calling group to the channel
--identified by the given prefix
function CalljoinGUI.BroadcastChannel(chatPrefix)
	local template = Calling.GetLocalized("channelAdvertise").form
	local callgroup = towstring(Calling.Channel)
	local msg = template:format(callgroup, callgroup)
	Calling.SendChat(chatPrefix, msg)	
end

--sends a request for invitation to an existing calling group to the channel
--identified by the given prefix
function CalljoinGUI.RequestBroadcast(chatPrefix)
	local msg = Calling.GetLocalized("channelRequest").form
	Calling.SendChat(chatPrefix, msg)
end

--brings up a popup inviting the user to the calling group given by channel
--reference to the given sender is included in the message.
function CalljoinGUI.ShowInvitation(sender, channel)
	local msg =  towstring(Calling.GetLocalized("channelAdvertGotten"):format(
		towstring(sender), towstring(channel)))
	DialogManager.MakeTwoButtonDialog(msg,
		Calling.GetLocalized("join"), function() Calling.RequestJoin(channel) end,
		Calling.GetLocalized("ignore"), nil)
end

--brings up a popup showing the group management options appropriate for
--being with or without calling group.
function CalljoinGUI.ContextSensitiveChannelOperation()
	if Calling.Channel ~= nil then
		CalljoinGUI.InChannelOptions()	
	else
		CalljoinGUI.NoChannelOptions()	
	end
end