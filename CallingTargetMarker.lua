-- Copyright (c) 2009, Kristian Bergmann
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
--   * Redistributions of source code must retain the above copyright notice,
--     this list of conditions and the following disclaimer.
--   * Redistributions in binary form must reproduce the above copyright
--     notice, this list of conditions and the following disclaimer in the
--     documentation and/or other materials provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

local eventsRegistered = false
--toggles marking of the current assist target (having this on is CPU intensive)
function Calling.SetShowMarker(show)
	CallingSettings.showTargetMarker = show
	
	if CallingSettings.showTargetMarker then
		--register event handlers, showing will be done by
		--Calling.UpdateTargetMarker
		if not eventsRegistered then
			RegisterEventHandler(SystemData.Events.PLAYER_TARGET_UPDATED, "Calling.OnTargetChanged")
			RegisterEventHandler(SystemData.Events.WORLD_OBJ_COMBAT_EVENT, "Calling.OnCombatEvent")
			eventsRegistered = true
		end
		
		Calling.Print(Calling.GetLocalized("markEnabled"))
	else
		--unregister event handlers
		if eventsRegistered then
			UnregisterEventHandler(SystemData.Events.PLAYER_TARGET_UPDATED, "Calling.OnTargetChanged")
			UnregisterEventHandler(SystemData.Events.WORLD_OBJ_COMBAT_EVENT, "Calling.OnCombatEvent")
			eventsRegistered = false
		end
		
		--detach and hide target marker
		if cur_eid ~= nil then
			DetachWindowFromWorldObject("CallingTargetMarker", cur_eid)
			cur_eid = nil
		end
		WindowSetShowing("CallingTargetMarker", false)
		Calling.SetTargetAlpha(0)
		
		Calling.Print(Calling.GetLocalized("markDisabled"))
	end
end

--returns true iff currently the target marker is set to be shown
function Calling.IsMarkerShown()
	return CallingSettings.showTargetMarker
end

local lastAlpha = -1
--Sets the target marker's alpha value.
--Visibility toggling is automatically handled on tranisitions between
--alpha == 0 and alpha != 0.
function Calling.SetTargetAlpha(alpha)
	if alpha ~= lastAlpha then
		if lastAlpha == 0 then
			WindowSetShowing("CallingTargetMarker",true)
		end
		
		if alpha == 0 then
			WindowSetShowing("CallingTargetMarker", false)
		else
			WindowSetAlpha("CallingTargetMarker", alpha)
		end
		
		lastAlpha = alpha
	end
end

--used to count updates without position changes (target vanished)
local lastX, lastY
local noPositionChange = 0
--remembers name of current target and its stripped version
local lastTarget = nil
local strippedTarget = nil

--attaches the target marker to the current assist target and lets it fade
--out if the target vanished.
function Calling.UpdateTargetMarker(elapsedTime)
	local eid = nil
	
	if Calling.lastCall ~= nil and Calling.lastCall.TargetIsPlayer then
		--update target name if it has changed
		if lastTarget ~= Calling.lastCall.Target then
			lastTarget = Calling.lastCall.Target
			strippedTarget = Calling.StripString(lastTarget)
		end		
		
		--see if client side object id is cached for the target
		eid = Calling.enemyNameToId[strippedTarget]
		
		--if no match is found (target probably npc), attach marker to
		--player to show that marker is present, but object id not known
		if eid == nil and Calling.sinceLastCall < Calling.callStaysFor then
			eid = GameData.Player.worldObjNum
			if Calling.sinceLastCall > Calling.callStaysFor - 1 then
				Calling.SetTargetAlpha(Calling.callStaysFor - Calling.sinceLastCall)
			end
		end
	end
	
	--update attachment of the marker to an ingame entity
	if (eid ~= cur_eid) then
		--detach and hide if cur_eid (still the actual one) is not null
		if (cur_eid) then
			DetachWindowFromWorldObject("CallingTargetMarker", cur_eid)
			Calling.SetTargetAlpha(0)
		end
		
		--reattach and show marker if the new entity is valid
		if eid and (eid > 0) then
			MoveWindowToWorldObject("CallingTargetMarker",eid,1)
			AttachWindowToWorldObject("CallingTargetMarker",eid)
			Calling.SetTargetAlpha(1)
		end
		
		--set saved entity id
		cur_eid = eid
	end

	--hide marker, if it did not move on screen for a longer time
	--bit of a HACK but i have no better idea
	local x,y = WindowGetScreenPosition("CallingTargetMarker")
	if lastX ~= x or lastY ~= y then
		noPositionChange = 0
		lastX = x
		lastY = y
	else
		noPositionChange = noPositionChange + elapsedTime
	end

	--starts fading out the target after some seconds without movement and 
	--finally hides it
	local posFadeOut = 7
	if noPositionChange > Calling.callStaysFor then
		Calling.SetTargetAlpha(0)
	elseif noPositionChange > Calling.callStaysFor - posFadeOut then
		Calling.SetTargetAlpha((Calling.callStaysFor - noPositionChange) /  posFadeOut)
	else
		Calling.SetTargetAlpha(1)
	end
end
