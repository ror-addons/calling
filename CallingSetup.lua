-- Copyright (c) 2009, Kristian Bergmann
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
--   * Redistributions of source code must retain the above copyright notice,
--     this list of conditions and the following disclaimer.
--   * Redistributions in binary form must reproduce the above copyright
--     notice, this list of conditions and the following disclaimer in the
--     documentation and/or other materials provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

local initialized = false
--initializes setup GUI components
function CallingSetup.Initialize()       
	--window title
	LabelSetText( "CallingSetupTitleBarText", L"Calling Setup" )

	--macro submenu setup--
	LabelSetText("CallingSetupMacroTitle", Calling.GetLocalized("macros") )
	LabelSetText("CallingSetupMacroCallTitle", L"Call" )
	LabelSetText("CallingSetupMacroTargetTitle", L"Target" )
	LabelSetText("CallingSetupMacroInstructions", Calling.GetLocalized("macroInstructions"))

	if WindowGetShowing( "CallingSetup" ) then
		CallingSetup.UpdateMacros()
	end

	--keybinding submenu setup--
	LabelSetText("CallingSetupBindTitle", Calling.GetLocalized("keybindings") )
	LabelSetText("CallingSetupBind1Title", Calling.GetLocalized("boundKey"))
	LabelSetText("CallingSetupBindSelectText", Calling.GetLocalized("bindSelectInstructions"))
	
	if (not initialized) then
		RegisterEventHandler (SystemData.Events.R_BUTTON_UP_PROCESSED, "CallingSetup.OnRButtonUp")  
	end

	CallingSetup.UpdateBindingTexts()    

	--priorities submenu setup--
	LabelSetText("CallingSetupPriosTitle", Calling.GetLocalized("priorities") )
	LabelSetText("CallingSetupPriosText", Calling.GetLocalized("prioInstructions"))
	--first priority labels
	LabelSetText("CallingSetupPrios_Prio1_Title", Calling.GetLocalized("prio1") )
	LabelSetText("CallingSetupPrios_Prio1_TypeLabel", Calling.GetLocalized("type") )
	LabelSetText("CallingSetupPrios_Prio1_SelectionLabel", Calling.GetLocalized("selection") )
	--second priority labels
	LabelSetText("CallingSetupPrios_Prio2_Title", Calling.GetLocalized("prio2") )
	LabelSetText("CallingSetupPrios_Prio2_TypeLabel", Calling.GetLocalized("type") )
	LabelSetText("CallingSetupPrios_Prio2_SelectionLabel", Calling.GetLocalized("selection") )
	--third priority labels
	LabelSetText("CallingSetupPrios_Prio3_Title", Calling.GetLocalized("prio3") )
	LabelSetText("CallingSetupPrios_Prio3_TypeLabel", Calling.GetLocalized("type") )
	LabelSetText("CallingSetupPrios_Prio3_SelectionLabel", Calling.GetLocalized("selection") )
	--initialize priority dropdown contents
	local prioTypes = Calling.GetLocalized("priorityTypes")
	if (not initialized) then
		for prio = 1, Calling.NUM_PRIOS do
			for i = 1,#prioTypes do
				ComboBoxAddMenuItem( "CallingSetupPrios_Prio"..prio.."_Type", prioTypes[i] )
			end
			ComboBoxAddMenuItem( "CallingSetupPrios_Prio"..prio.."_Selection", Calling.GetLocalized("choosePrioType"))
		end
	end

	--visibility submenu setup--
	LabelSetText( "CallingSetupShowTitle",  Calling.GetLocalized("display"))
	LabelSetText("CallingSetupShowCallingListLabel", Calling.GetLocalized("showCallList"))
		ButtonSetPressedFlag("CallingSetupShowCallingListButton", CallingSettings.CallsListShown)
	LabelSetText("CallingSetupShowKillsListLabel", Calling.GetLocalized("showKillList"))
		ButtonSetPressedFlag("CallingSetupShowKillsListButton", CallingSettings.KillsListShown)
	LabelSetText("CallingSetupShowTargetIconLabel", Calling.GetLocalized("showTargetClass"))
		ButtonSetPressedFlag("CallingSetupShowTargetIconButton", CallingSettings.TargetCareerShown)
	LabelSetText("CallingSetupShowNotificationColorLabel", Calling.GetLocalized("notificationCareerColor"))
		ButtonSetPressedFlag("CallingSetupShowNotificationColorButton", CallingSettings.NotificationCareerColor)
	LabelSetText("CallingSetupShowCallerIconLabel", Calling.GetLocalized("showCallerClass"))
		ButtonSetPressedFlag("CallingSetupShowCallerIconButton", CallingSettings.CallerCareerShown)
	LabelSetText("CallingSetupShow_LanguageLabel", Calling.GetLocalized("language"))
	CallingSetup.FillLanguageCombo()
	CallingSetup.SetLanguageSelection();

	--marking and dead target submenu removal setup--
	LabelSetText( "CallingSetupTargetingTitle",  Calling.GetLocalized("targeting"))
	LabelSetText("CallingSetupTargetingShowTargetMarkerLabel", Calling.GetLocalized("showTargetMarker"))
		ButtonSetPressedFlag("CallingSetupTargetingShowTargetMarkerButton", Calling.IsMarkerShown())
	LabelSetText("CallingSetupTargetingEvaluateRvrKillsLabel", Calling.GetLocalized("evaluateRvrKills"))
		ButtonSetPressedFlag("CallingSetupTargetingEvaluateRvrKillsButton", CallingSettings.EvaluateRvrKills)
	LabelSetText("CallingSetupTargetingWithdrawNPCLabel", Calling.GetLocalized("removeNpc"))
		ButtonSetPressedFlag("CallingSetupTargetingWithdrawNPCButton", CallingSettings.RemoveDeadNPC)
	
	-- group management submenu setup--
	LabelSetText( "CallingSetupGroupTitle",  Calling.GetLocalized("groupMgmt"))
	--group invitation popup options
	LabelSetText("CallingSetupGroupInviteLabel", Calling.GetLocalized("inviteDesc"))
	LabelSetText("CallingSetupGroupShowInvitesLabel", Calling.GetLocalized("showGroupInvites"))
		ButtonSetPressedFlag("CallingSetupGroupShowInvitesButton", CallingSettings.ShowGroupInvites)
	LabelSetText("CallingSetupGroupSupressInvitesInGroupLabel", Calling.GetLocalized("showInvitesWhenGrouped"))
		ButtonSetPressedFlag("CallingSetupGroupSupressInvitesInGroupButton", CallingSettings.SupressInvitesWhenGrouped)
	--options for answering group invitation requests
	LabelSetText("CallingSetupGroupInviteOnRequestLabel", Calling.GetLocalized("inviteOnRequestDesc"))
	LabelSetText("CallingSetupGroupInviteOnRequestGALabel", Calling.GetLocalized("reactOnInvitationRequestsGnA"))
		ButtonSetPressedFlag("CallingSetupGroupInviteOnRequestGAButton", CallingSettings.ReactOnInvitationRequestsGnA)
	LabelSetText("CallingSetupGroupInviteOnRequestPWLabel", Calling.GetLocalized("reactOnInvitationRequestsPnW"))
		ButtonSetPressedFlag("CallingSetupGroupInviteOnRequestPWButton", CallingSettings.ReactOnInvitationRequestsPnW)
	LabelSetText("CallingSetupGroupInviteOnRequestLLabel", Calling.GetLocalized("reactOnInvitationRequestsL"))
		ButtonSetPressedFlag("CallingSetupGroupInviteOnRequestLButton", CallingSettings.ReactOnInvitationRequestsL)
		
	--tutorial "submenu" setup--
	LabelSetText("CallingSetupTutorialTitle", Calling.GetLocalized("tutorial"))
	LabelSetText("CallingSetupTutorialDescription", Calling.GetLocalized("tutDescription"))
	LabelSetText("CallingSetupTutorialTutTitle", L"1: "..Calling.GetLocalized("tutParts")[1].Title)
	ButtonSetText( "CallingSetupTutorialTutPrev", L"<-" )
	ButtonSetText( "CallingSetupTutorialTutNext", L"->" )
	LabelSetText("CallingSetupTutorialTutText", Calling.GetLocalized("tutParts")[1].Text)
	
	--submenu chooser--
	LabelSetText( "CallingSetupCategoriesTitle",  Calling.GetLocalized("subMenus"))
	ButtonSetText( "CallingSetupCategoriesMacro", Calling.GetLocalized("macros") )
	ButtonSetText( "CallingSetupCategoriesBind", Calling.GetLocalized("keybindings") )
	ButtonSetText( "CallingSetupCategoriesPrios", Calling.GetLocalized("priorities") )
	ButtonSetText( "CallingSetupCategoriesShow", Calling.GetLocalized("display") )
	ButtonSetText( "CallingSetupCategoriesGroup", Calling.GetLocalized("groupMgmt") )
	ButtonSetText( "CallingSetupCategoriesTargeting", Calling.GetLocalized("targeting") )
	ButtonSetText( "CallingSetupCategoriesTutorial", Calling.GetLocalized("tutorial") )
	
	CallingSetup.ManageShownSettings()
	
	initialized = true;
end

------------------------------------
-- SETTINGS WINDOW SHOWING/HIDING --
------------------------------------

--toggles visibility of the settings window
function CallingSetup.ToggleShowing()
	local showing = WindowGetShowing( "CallingSetup" )
	if not showing then
		CallingSetup.Show()
	else
		CallingSetup.Hide()
	end
end

--hides the settings window
function CallingSetup.Hide()
    CallingSetup.HideWindow()
end

--hides the settings window
function CallingSetup.HideWindow()
    local showing = WindowGetShowing( "CallingSetup" )
    if(showing == true) then
        WindowSetShowing( "CallingSetup", false )
    end
end

--on hidden event handler
function CallingSetup.OnHidden()
    WindowUtils.OnHidden()
    CallingSetup.HideWindow()
end

--shows the settings window
function CallingSetup.Show()
	local showing = WindowGetShowing( "CallingSetup" )
	if not showing then
		CallingSetup.UpdateMacros()
		WindowSetShowing( "CallingSetup", true )
		--Calling.RequestUserList() --TODO should not be necessary anymore
		CallingSetup.DropdownsFromPriorities()
	end
end

--on shown event handler
function CallingSetup.OnShown()
    WindowUtils.OnShown(CallingSetup.Hide, WindowUtils.Cascade.MODE_AUTOMATIC)
end

-----------------------
-- CALLING MINI-ICON --
-----------------------
CallingIcon = {}

--shows a tooltip describing the calling icon's functions
function CallingIcon.OnMouseOver()
	Tooltips.CreateTextOnlyTooltip(SystemData.ActiveWindow.name)
	Tooltips.SetTooltipText(1, 1, Calling.GetLocalized("groupManagement"))
	Tooltips.SetTooltipText(2, 1, Calling.GetLocalized("settingsShownToggle"))
	if Calling.Channel then
		Tooltips.SetTooltipText(4, 1, Calling.GetLocalized("groupFounder")..Calling.Channel)
	end
	Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_LEFT)
	Tooltips.Finalize()
end

------------------------------------
-- GENERAL SETTINGS FUNCTIONALITY --
------------------------------------

--toggles the check for the currently active window
function CallingSetup.ToggleActiveCheckbox()
	local windowName = SystemData.ActiveWindow.name
	local buttonName = windowName.."Button"
	local setPressed = not ButtonGetPressedFlag(buttonName)
	ButtonSetPressedFlag(buttonName, setPressed)
--	Calling.d(windowName..": ", setPressed)
	return setPressed
end

-- OnShutdown Handler
function CallingSetup.Shutdown()
	--empty
end

-----------------------
-- SUBMENU SELECTION --
-----------------------

--public methods for selection of one of the submenus
function CallingSetup.ChooseMacro() CallingSetup.ManageShownSettings("Macro") end
function CallingSetup.ChooseBind() CallingSetup.ManageShownSettings("Bind") end
function CallingSetup.ChoosePrios() CallingSetup.ManageShownSettings("Prios") end
function CallingSetup.ChooseShow() CallingSetup.ManageShownSettings("Show") end
function CallingSetup.ChooseGroup() CallingSetup.ManageShownSettings("Group") end
function CallingSetup.ChooseTargeting() CallingSetup.ManageShownSettings("Targeting") end
function CallingSetup.ChooseTutorial() CallingSetup.ManageShownSettings("Tutorial") end

--ensures at most one submenu is shown and toggles visibility of
--the submenus accordingly
function CallingSetup.ManageShownSettings(shown)
	--pick the submenu to be shown
	if shown ~= nil then
		CallingSettings.showMacroSettings = (shown == "Macro")
		CallingSettings.showKeybindingSettings = (shown == "Bind")
		CallingSettings.showPrioSettings = (shown == "Prios")
		CallingSettings.showVisibilitySettings = (shown == "Show")
		CallingSettings.showGroupManagementSettings = (shown == "Group")
		CallingSettings.showTargetingSettings = (shown == "Targeting")
		CallingSettings.showTutorial = (shown == "Tutorial")
	end
	
	--refresh calling channel user list for priority submenu
	if CallingSettings.showPrioSettings then
		CallingChat.wantUserlistUpdate  = true
	end
	
	--toggle visibility of the different submenus
	CallingSetup.EnableSubMenu("Macro", CallingSettings.showMacroSettings)
	CallingSetup.EnableSubMenu("Bind", CallingSettings.showKeybindingSettings)
	CallingSetup.EnableSubMenu("Prios", CallingSettings.showPrioSettings)
	CallingSetup.EnableSubMenu("Show", CallingSettings.showVisibilitySettings)
	CallingSetup.EnableSubMenu("Group", CallingSettings.showGroupManagementSettings)
	CallingSetup.EnableSubMenu("Targeting", CallingSettings.showTargetingSettings)
	CallingSetup.EnableSubMenu("Tutorial", CallingSettings.showTutorial)
end

--sets the visibility of the given submenu to the boolean val
function CallingSetup.EnableSubMenu( subName, val)
	WindowSetShowing( "CallingSetup"..subName, val )
	ButtonSetPressedFlag( "CallingSetupCategories"..subName, val )
	ButtonSetStayDownFlag( "CallingSetupCategories"..subName, val )
end

------------
-- MACROS --
------------

--creates call and target macros if necessary and puts drag and drop sources
--into the appropriate icon slots of the macro submenu
function CallingSetup.UpdateMacros()
	--call macro
	local callMacroId = CreateMacro(L"Call", L"/script Calling.Call()", 214)
	local callMacro = GetMacro(callMacroId)
	WindowSetId("CallingSetupMacroIconSlotC", callMacroId)
	local texture, x, y = GetIconData( callMacro.iconNum)
	DynamicImageSetTexture( "CallingSetupMacroIconSlotCIconBase", texture, x, y )
	SetMacroData( callMacro.name, callMacro.text, callMacro.iconNum, callMacroId)

	--target macro
	local targetMacroId = CreateMacro(L"Target", L"/script Calling.Target()", 210)
	local targetMacro = GetMacro(targetMacroId)
	WindowSetId("CallingSetupMacroIconSlotT", targetMacroId)
	local texture, x, y = GetIconData( targetMacro.iconNum)
	DynamicImageSetTexture( "CallingSetupMacroIconSlotTIconBase", texture, x, y )
	SetMacroData( targetMacro.name, targetMacro.text, targetMacro.iconNum, targetMacroId)
end

---------------
-- KEYBINDIG --
---------------

--sets the target action bar and action slot indices for the keybinding
--submenu and updates the associated helper texts
function CallingSetup.OnRButtonUp()
    local button = FrameManager:GetMouseOverWindow ()
    
    -- It's probably an action button if these conditions are true
    if (button ~= nil and button.GetSlot ~= nil)
    then
        local hotbarSlot = button:GetSlot ()
        
	--get bar and slot and action indices
        CallingKeybind.bar = math.floor((hotbarSlot - 1) / 12) + 1
        CallingKeybind.btn = ((hotbarSlot - 1) % 12) + 1
        CallingKeybind.logicBar = ActionBars.m_Bars[CallingKeybind.bar].m_PageSelectorWindow.m_LogicalPage
        local actionIndex = (CallingKeybind.logicBar-1) * 12 + CallingKeybind.btn

	--set action for keybinding
        CallingKeybind.action = "ACTION_BAR_"..actionIndex
	for k, v in pairs(KeyMappingWindow.actionsData) do
		if v.action == CallingKeybind.action then
			CallingKeybind.actionDataIndex = k
			KeyMappingWindow.SelectedActionDataIndex = k
		end    
	end 
        
        --update helper texts
	CallingSetup.UpdateBindingTexts()
    end
end

--starts the keybinding procedure
function CallingSetup.StartBinding1()
	CallingSetup.StartBinding(1)
end

--data necessary for the keybinding process
CallingKeybind = {
	--selected physical bar index
	bar = -1,
	--selected logical bar index inside the physical bar
	logicBar = -1,
	--selected button index
	btn = -1,
	--serial index associated with the selected action slot
	actionDataIndex = -1,
	--string identifier of the selected action slot
	action = ""
}

--sets the keybinding helper texts according to the current stage
--of the keybinding process
function CallingSetup.UpdateBindingTexts()
	if CallingKeybind.bar <= 0 or CallingKeybind.btn <= 0 then
	    LabelSetText("CallingSetupBindTargetText", Calling.GetLocalized("noSlotSelected"))
	    ButtonSetText("CallingSetupBindBtn1", Calling.GetLocalized("selectSlot"))
	    LabelSetText("CallingSetupBindButtonText", Calling.GetLocalized("selectSlotToProceed"))
	else 
		LabelSetText("CallingSetupBindTargetText",
			Calling.GetLocalized("selectedSlot") ..CallingKeybind.btn.. Calling.GetLocalized("inBar") ..CallingKeybind.logicBar..L".")
		ButtonSetText("CallingSetupBindBtn1", KeyMappingWindow.actionsData[ CallingKeybind.actionDataIndex ].keys1.name)
		if KeyMappingWindow.bindMode then
			LabelSetText("CallingSetupBindButtonText", Calling.GetLocalized("bindInstructions"))
		else
			LabelSetText("CallingSetupBindButtonText", Calling.GetLocalized("bindClickInstructions") )
		end
	end
end

---------------
-- PRIORITES --
---------------

--updates the priority dropdowns' contents on changing of one of
--the priority types.
function CallingSetup.OnPrioTypeChanged()
	CallingSetup.UpdateDropdowns()
end

--sets the priority settings according to the current selections
--in the configuration dropdowns
function CallingSetup.OnPrioSelectionChanged()
	CallingSetup.PrioritiesFromDropdowns()
end

local comboBoxContents = {}
--fills the given comboBox with all current users of the calling
--channel and adds the given containedCaller if necessary.
--This ensures that the last selected caller can remain selected.
function CallingSetup.FillWithCallers(comboBox, containedCaller)
	local contents = {}
	--add users to the combo box
	for i = 1, #Calling.UserList do
		ComboBoxAddMenuItem(comboBox, Calling.UserList[i])
		contents[i] = Calling.UserList[i]
	end

	--cache contents of the box
	comboBoxContents[comboBox] = contents

	--add containedCaller if necessary
	if containedCaller ~= nil then
		CallingSetup.EnsureComboBoxContainsEntry( comboBox, containedCaller )
	end
end

--fills the given comboBox with the character archetypes
function CallingSetup.FillWithCharacterArchetypes(comboBox)
	local contents = {}
	--add archetypes
	local archetypesStrings = Calling.GetLocalized("characterTypes")
	for i = 1, Calling.Archetypes.Count do
		ComboBoxAddMenuItem(comboBox, archetypesStrings[i])
		contents[i] = archetypesStrings[i]
	end
	
	--cache content of the box
	comboBoxContents[comboBox] = contents
end

local comboCareerIDs = {}
--fills the given combobox with the career strings of the given
--realm
function CallingSetup.FillWithCareers(comboBox, realm)
	local contents = {}
	local careerLines = GameData.CareerLine
	local count = 1
	--filters the realm's careers from the careers and adds
	--them
	for name, id in pairs(careerLines) do
		local pairing = math.floor((id - 1) / 8)
		local careerRealm = math.mod( math.floor((id - 1) / 4) ,  2) + 1
		local careerSlot = 11 + pairing * 40 + id
		local careerName = GetCareerName(careerSlot, 1)
		if realm == careerRealm then
			--remember id's of the entered careers
			if comboCareerIDs[comboBox] == nil then
				comboCareerIDs[comboBox] = {}
			end
			comboCareerIDs[comboBox][count] = id
			
			--add career to the box
			contents[count] = careerName
			count = count + 1
			ComboBoxAddMenuItem(comboBox, careerName)
		end
	end
	
	--cache contents of the box
	comboBoxContents[comboBox] = contents
end

--fills the given combo box with the strings corresponding to the Calling.Grouptypes
function CallingSetup.FillWithGroupTypes(comboBox)
	local contents = {}
	--add grouptypes
	local grouptypesStrings = Calling.GetLocalized("groupTypes")
	for i = 1, Calling.Grouptypes.Count do
		ComboBoxAddMenuItem(comboBox, grouptypesStrings[i])
		contents[i] = grouptypesStrings[i]
	end
	
	--cache content of the box
	comboBoxContents[comboBox] = contents	
end

--updates all contents of the priority dropdown boxes and sets the priority
--settings accordingly.
function CallingSetup.UpdateDropdowns()
	for i = 1, Calling.NUM_PRIOS do
		--set up some convenience variables
		local entryWnd = "CallingSetupPrios_Prio"..i
		local comboName = entryWnd.."_Selection"
		local playerRealm = GameData.Player.realm
		local enemyRealm = math.mod(playerRealm, 2) + 1

		--remember priority type, selected identifier for the type and the
		--selection's name
		local selectedPrio = ComboBoxGetSelectedMenuItem( entryWnd.."_Type" )
		local selectedId = ComboBoxGetSelectedMenuItem( entryWnd.."_Selection" )
		local text
		if selectedId > 0 then
			text = ComboBoxGetSelectedText( comboName )
		end
		
		--clear selection
		ComboBoxClearMenuItems( comboName )

		--fill selection combo box with content based on priority type
		if selectedPrio == Calling.Priority.CALLER then
			CallingSetup.FillWithCallers( comboName, text )
		elseif selectedPrio == Calling.Priority.CALLER_TYPE then
			CallingSetup.FillWithCharacterArchetypes( comboName )
		elseif selectedPrio == Calling.Priority.CALLER_CAREER then
			CallingSetup.FillWithCareers( comboName , playerRealm)
		elseif selectedPrio == Calling.Priority.TARGET_TYPE then
			CallingSetup.FillWithCharacterArchetypes( comboName )
		elseif selectedPrio == Calling.Priority.TARGET_CAREER then
			CallingSetup.FillWithCareers( comboName , enemyRealm)
		elseif selectedPrio == Calling.Priority.CALLER_GROUP then
			CallingSetup.FillWithGroupTypes( comboName )
		end

		--reselect previous selection based on it's text
		local contents = comboBoxContents[comboName]
		if contents ~= nil then
			for j = 1, #contents do
				if contents[j] == text then
					ComboBoxSetSelectedMenuItem(comboName, j)
				end
			end
		end
	end
	
	CallingSetup.PrioritiesFromDropdowns()
end

--Transfers the priority settings from the dropdowns to the CallingSettings
--(see CallingSettings.Priorities documentation in Calling.lua, too)
function CallingSetup.PrioritiesFromDropdowns()
	CallingSettings.Priorities = {}
	for i = 1, Calling.NUM_PRIOS do
		--setup convenience variables
		local entryWnd = "CallingSetupPrios_Prio"..i
		local typeId = ComboBoxGetSelectedMenuItem( entryWnd.."_Type" )
		local selectionId = ComboBoxGetSelectedMenuItem( entryWnd.."_Selection" )
		
		--setup priority from dropdown
		if typeId ~= nil then
			--set priority type
			local prio = {
				Type = typeId,
			}
			
			--set priority selection based on type
			if selectionId ~= nil and selectionId > 0 then
				if prio.Type == Calling.Priority.CALLER then
					prio.Selection = ComboBoxGetSelectedText( entryWnd.."_Selection" )
				elseif  prio.Type == Calling.Priority.CALLER_TYPE or
					prio.Type == Calling.Priority.TARGET_TYPE or
					prio.Type == Calling.Priority.CALLER_GROUP 
				then
d(L"selectionFromDropdown")
					prio.Selection = selectionId
				elseif  prio.Type == Calling.Priority.CALLER_CAREER or
					prio.Type == Calling.Priority.TARGET_CAREER
				then
					prio.Selection = comboCareerIDs[entryWnd.."_Selection"][selectionId]
				end
			end
			
			--enter created priority info into the settings
			CallingSettings.Priorities[i] = prio
		end
	end
end

--sets up the priority dropdowns in order to match the CallingSettings
--(see CallingSettings.Priorities documentation in Calling.lua, too)
function CallingSetup.DropdownsFromPriorities()
	if CallingSettings.Priorities == nil then
		CallingSettings.Priorities = {}
	end
	local priorities = CallingSettings.Priorities
	
	--set priority type dropdowns
	for i = 1, Calling.NUM_PRIOS do
		local prio = priorities[i]
		local comboName = "CallingSetupPrios_Prio"..i.."_Type"
		if prio ~= nil and prio.Type ~= nil then
			ComboBoxSetSelectedMenuItem( comboName, prio.Type)
		end
	end
	
	--setup priority selection drowpdowns in order to match priority types
	CallingSetup.UpdateDropdowns()
	
	for i = 1, Calling.NUM_PRIOS do
		local prio = priorities[i]
		local comboName = "CallingSetupPrios_Prio"..i.."_Selection"
		
		
		if  prio ~= nil and prio.Selection ~= nil and prio.Type ~= nil then
			--set selection based on priority type
			if  prio.Type == Calling.Priority.CALLER_TYPE or
				prio.Type == Calling.Priority.TARGET_TYPE or
				prio.Type == Calling.Priority.CALLER_GROUP 
			then
				ComboBoxSetSelectedMenuItem( comboName, prio.Selection)
			elseif  prio.Type == Calling.Priority.CALLER_CAREER or
					prio.Type == Calling.Priority.TARGET_CAREER
			then
				local careerLine = prio.Selection
				local combo2Career = comboCareerIDs[comboName]
				for j = 1, #combo2Career do
					if combo2Career[j] == careerLine then
						ComboBoxSetSelectedMenuItem( comboName, j )
					end
				end
			elseif prio.Type == Calling.Priority.CALLER then
				local index = CallingSetup.EnsureComboBoxContainsEntry( comboName, prio.Selection )
				ComboBoxSetSelectedMenuItem( comboName, index )
			end
		end
	end
	
	-- finally insert constructed priorities into actual settings
	CallingSettings.Priorities = priorities
end

--adds the given entry to the given combobox if not already contained.
--returns the index of entry.
function CallingSetup.EnsureComboBoxContainsEntry( comboName, entry )
	local found = -1
	local entries = comboBoxContents[comboName]
	
	--search for existing entry
	for j = 1,#entries do
		if entries[j] == entry then
			found = j			
		end
	end
	
	--add entry, if not existing
	if found == -1 then
		ComboBoxAddMenuItem( comboName, entry )
		found = #entries + 1
		entries[found] = entry
	end
	
	--return index of entry
	return found
end

------------------------
-- VISIBILITY OPTIONS --
------------------------

--toggles showing the calls list
function CallingSetup.OnShowCallingListClick()
	CallingSettings.CallsListShown = CallingSetup.ToggleActiveCheckbox()
	Calling.ManageListsShowing()
end

--toggles showing the kills list
function CallingSetup.OnShowKillsListClick()
	CallingSettings.KillsListShown = CallingSetup.ToggleActiveCheckbox()
	Calling.ManageListsShowing()
end

--toggles showing the target career's icon in the calling notification
function CallingSetup.OnShowTargetIconClick()
	CallingSettings.TargetCareerShown =	CallingSetup.ToggleActiveCheckbox()
	if not CallingSettings.TargetCareerShown then
		WindowSetAlpha( "CallWindowIcon", 0 )
	end
end

--toggles showing the caller career's icon in the calling notification
function CallingSetup.OnShowCallerIconClick()
	CallingSettings.CallerCareerShown =	CallingSetup.ToggleActiveCheckbox()
	if not CallingSettings.CallerCareerShown then
		WindowSetAlpha( "CallWindowCallerIcon", 0 )
	end
end

function CallingSetup.OnNotificationColorClick()
	CallingSettings.NotificationCareerColor = CallingSetup.ToggleActiveCheckbox()
end

local languageTexts = {}
--fills the language combo box of the calling setup window with all localized
--languages and the default of the active language.
function CallingSetup.FillLanguageCombo()
	languageTexts[-1] = Calling.GetLocalized("activeLanguage");
	for k, v in pairs(Calling.localization) do
		languageTexts[k] = v.languageName;
	end
	
	local combo = "CallingSetupShow_Language";
	ComboBoxClearMenuItems(combo)
	for k, v in pairs(languageTexts) do
		ComboBoxAddMenuItem(combo, v)
	end
end

--selects the currently set language from the initialized language dropdown
function CallingSetup.SetLanguageSelection()
	local index = 1
	local combo = "CallingSetupShow_Language"
	for k, v in pairs(languageTexts) do
		if (k == CallingSettings.language) then
			ComboBoxSetSelectedMenuItem(combo, index)
		end
		index = index + 1
	end
end

--toggles the language of Calling
function CallingSetup.OnLanguageChanged()
	local index = 1
	local combo = "CallingSetupShow_Language"
	local selected = ComboBoxGetSelectedMenuItem(combo)
	for k, v in pairs(languageTexts) do
		if (index == selected) then
			CallingSettings.language = k
		end
		index = index + 1
	end
	
	CallingSetup.Initialize()
end
-----------------------
-- TARGETING OPTIONS --
-----------------------

--toggles showing the target marker (having this on is CPU intensive)
function CallingSetup.OnShowTargetMarkerClick()
	local checked = CallingSetup.ToggleActiveCheckbox()
	Calling.SetShowMarker(checked)
end

--toggles removing of killed player targets (local)
--(having this on is CPU intensive)
function CallingSetup.OnEvaluateRvrKillsClick()
	local checked = CallingSetup.ToggleActiveCheckbox()
	Calling.SetEvaluateRvrKills(checked)
end

--toggles withdrawing of calls on now killed NPC (for whole group)
function CallingSetup.OnWithdrawNpcClick()
	CallingSettings.RemoveDeadNPC = CallingSetup.ToggleActiveCheckbox()
end

------------------------------
-- GROUP MANAGEMENT OPTIONS --
------------------------------

--toggles showing an invitation popup when invited to a calling group 
function CallingSetup.OnShowGroupInvitesClick()
	CallingSettings.ShowGroupInvites = CallingSetup.ToggleActiveCheckbox()
end

--toggles supression of group invitation popup, if player is already part of
--a calling group
function CallingSetup.OnShowInvitesInGroupClick()
	CallingSettings.SupressInvitesWhenGrouped = CallingSetup.ToggleActiveCheckbox()
end

--toggles automatic calling group invitation for requests from within guild
--and alliance chat channels
function CallingSetup.OnInviteOnRequestGAClick()
	CallingSettings.ReactOnInvitationRequestsGnA = CallingSetup.ToggleActiveCheckbox()
end

--toggles automatic calling group invitation for requests from within party
--and warband chat channels
function CallingSetup.OnInviteOnRequestPWClick()
	CallingSettings.ReactOnInvitationRequestsPnW = CallingSetup.ToggleActiveCheckbox()
end

--toggles automatic calling group invitation for requests from local chat
function CallingSetup.OnInviteOnRequestLClick()
	CallingSettings.ReactOnInvitationRequestsL = CallingSetup.ToggleActiveCheckbox()
end

----------------------
-- TUTORIAL SUBMENU --
----------------------

--currently shown tutorial
local tutorialPart = 1

--sets the shown tutorial text to the one with the given id
function CallingSetup.ShowTutorialPart(id)
	local tutParts = Calling.GetLocalized("tutParts")
	if tutParts[id] then
		tutorialPart = id
		LabelSetText("CallingSetupTutorialTutTitle", id..L": "..tutParts[tutorialPart].Title)
		LabelSetText("CallingSetupTutorialTutText", tutParts[tutorialPart].Text)
	end
end

--brings up the previous tutorial entry
function CallingSetup.OnTutPrevClick()
	CallingSetup.ShowTutorialPart(tutorialPart - 1)
end

--brings up the next tutorial entry
function CallingSetup.OnTutNextClick()
	CallingSetup.ShowTutorialPart(tutorialPart + 1)
end

--toggles auto assist option
function CallingSetup.ToggleAutoAssist()
	CallingSetup.SetAutoAssist(not CallingSettings.AutoAssist)
end

function CallingSetup.SetAutoAssist(auto)
	--if CallingSettings.AutoAssist==nil then CallingSettings.AutoAssist=false end
	CallingSettings.AutoAssist=auto

	if CallingSettings.AutoAssist then
		Calling.Print(Calling.GetLocalized("autoEnabled"))
	else
		Calling.Print(Calling.GetLocalized("autoDisabled"))
	end
	if Calling.Channel then
		if not CallingSettings.AutoAssist then
			DynamicImageSetTexture( "CallingIcon",  "calling_active", 0, 0 )
		else
			DynamicImageSetTexture( "CallingIcon",  "calling_auto", 0, 0 )
		end
	end
end
