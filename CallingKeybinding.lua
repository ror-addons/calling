--This code is mainly taken from WARs standard keybinding window
-- Documentation is taken from there

local MAX_SHOWN_BINDINGS = 2

local KEY_ORDER_FIRST = 1
local KEY_ORDER_SECOND = 2
local KEY_ORDER_THIRD = 3
local KEY_ORDER_LAST = 4

local LCTRL = 29
local RCTRL = 157
local LSHIFT = 42
local RSHIFT = 54
local LALT = 56
local RALT = 184

local modiferKeyTableOrder =
{
    [LCTRL]     = KEY_ORDER_SECOND,
    [RCTRL]     = KEY_ORDER_SECOND,
    
    [LSHIFT]    = KEY_ORDER_THIRD,
    [RSHIFT]    = KEY_ORDER_THIRD,
    
    [LALT]      = KEY_ORDER_FIRST,
    [RALT]      = KEY_ORDER_FIRST,
}

local modiferKeyOppositeLookup =
{
    [LCTRL]     = RCTRL,
    [RCTRL]     = LCTRL,
    
    [LSHIFT]    = RSHIFT,
    [RSHIFT]    = LSHIFT,
    
    [LALT]      = RALT,
    [RALT]      = LALT,
}

local function IsOppositeModifierKeyInTable( keyTable, itemId )
    if( IsModifierKey( itemId ) )
    then
        return keyTable[ modiferKeyOppositeLookup[ itemId ] ] ~= nil
    else
        return false
    end
end

local function SwapBindings( bindingIndex )
    local table1 = { name=KeyMappingWindow.actionsData[ bindingIndex ].keys1.name,
                     buttons=KeyMappingWindow.actionsData[ bindingIndex ].keys1.buttons,
                     deviceId=KeyMappingWindow.actionsData[ bindingIndex ].keys1.deviceId }
                        
    local table2 = { name=KeyMappingWindow.actionsData[ bindingIndex ].keys2.name,
                     buttons=KeyMappingWindow.actionsData[ bindingIndex ].keys2.buttons,
                     deviceId=KeyMappingWindow.actionsData[ bindingIndex ].keys2.deviceId }
    
    KeyMappingWindow.actionsData[ bindingIndex ].keys1 = table2
    KeyMappingWindow.actionsData[ bindingIndex ].keys2 = table1
end

local function ClearBindingData( bindingIndex, keyIndex )
    KeyMappingWindow.actionsData[ bindingIndex ]["keys"..keyIndex] = {}
    KeyMappingWindow.actionsData[ bindingIndex ]["keys"..keyIndex].name = noKeyLabel
    KeyMappingWindow.actionsData[ bindingIndex ]["keys"..keyIndex].buttons = {}
    KeyMappingWindow.actionsData[ bindingIndex ]["keys"..keyIndex].deviceId = 0
end

local function UpdateSingleBindingForAction( actionName, keyBindingName )
    local index = 1
    for action, key in pairs( SystemData.Settings.Keybindings )
    do  
        if( StringTables.BindableActions[ action ] )
        then
            if( actionName == KeyMappingWindow.actionsData[ index ].action )
            then
                local bindingData = {}
                KeyUtils.GetBindingsForAction( action, bindingData )
                
                local keyTextIndex = 1
                local noKey = false
                for keyIndex, binding in ipairs( bindingData )
                do
                    if( keyIndex > MAX_SHOWN_BINDINGS ) -- only show the first 2 bindings
                    then
                        break
                    end
                    
                    KeyMappingWindow.actionsData[ index ]["keys"..keyIndex].buttons = binding.buttons
                    KeyMappingWindow.actionsData[ index ]["keys"..keyIndex].deviceId = binding.deviceId
                    if( binding.name ~= L"" )
                    then
                        KeyMappingWindow.actionsData[ index ]["keys"..keyIndex].name = binding.name
                    else
                        noKey = true
                        KeyMappingWindow.actionsData[ index ]["keys"..keyIndex].name = noKeyLabel
                    end
                    
                    -- !!HACK!! switch the order of the buttons shown to get around the unordered map problem
                    if( keyIndex == MAX_SHOWN_BINDINGS and not noKey and buttonBound ~= 1 )
                    then
                        SwapBindings( index )
                    end
                    
                    keyTextIndex = keyTextIndex + 1
                end
                
                -- Set the text for the unbound keys
                for keyIndex=keyTextIndex, MAX_SHOWN_BINDINGS
                do
                    ClearBindingData( index, keyIndex )
                end
            elseif( KeyMappingWindow.actionsData[ index ].keys1.name == keyBindingName and keyBindingName ~= noKeyLabel )
            then
                ClearBindingData( index, 1 )
                if( KeyMappingWindow.actionsData[ index ].keys2.name ~= noKeyLabel )
                then
                    SwapBindings( index )
                end
            elseif( KeyMappingWindow.actionsData[ index ].keys2.name == keyBindingName and keyBindingName ~= noKeyLabel )
            then
                ClearBindingData( index, 2 )
            end
            
            index = index + 1
        end
    end
end

local function FilterActionList()
    KeyMappingWindow.actionListOrder = {}   
    for dataIndex, data in ipairs( KeyMappingWindow.actionsData ) do
        if( data.category == KeyMappingWindow.selectedCategory )
        then
            table.insert(KeyMappingWindow.actionListOrder, dataIndex)
        end
    end
    table.sort( KeyMappingWindow.actionListOrder, CompareActions )
end

local function UpdateActionList()
    FilterActionList()
    ListBoxSetDisplayOrder( "KeyMappingWindowActionsList", KeyMappingWindow.actionListOrder )
end

function CallingSetup.StartBinding( buttonNum )
    if( KeyMappingWindow.bindingButtonName == SystemData.MouseOverWindow.name or
        SystemData.MouseOverWindow.name ~= SystemData.ActiveWindow.name )
    then
        if( KeyMappingWindow.bindMode and KeyMappingWindow.bindingButtonName == SystemData.MouseOverWindow.name )
        then
            KeyMappingWindow.OnExitBindingMode(0, 0, 0, true)
        end
        return
    end

    -- Get us out of binding mode if we were in it
    if( KeyMappingWindow.bindMode and KeyMappingWindow.bindingButtonName ~= "" )
    then
        KeyMappingWindow.OnExitBindingMode()
    end

    -- Put us in bindmode
    KeyMappingWindow.bindMode = true
    
    -- Init the data we will use
    KeyMappingWindow.bindingButtonName = "CallingSetupBindBtn"..buttonNum
    KeyMappingWindow.bindingKeyName = L""
    KeyMappingWindow.selectedKeys = {}
    KeyMappingWindow.bindIndex = buttonNum
    
    -- Check the button
    ButtonSetCheckButtonFlag( KeyMappingWindow.bindingButtonName, true )
    ButtonSetPressedFlag( KeyMappingWindow.bindingButtonName, true )
    ButtonSetStayDownFlag( KeyMappingWindow.bindingButtonName, true )
    
    -- Set the help text
    LabelSetText( "KeyMappingWindowHelpText", GetString( StringTables.Default.TEXT_KEYBINDING_BINDMODE_HELP ) )
    
    -- Register for events so we can get the next key stroke
    WindowRegisterCoreEventHandler( KeyMappingWindow.bindingButtonName, "OnRawDeviceInput", "CallingSetup.OnRawDeviceInput" )
    WindowRegisterEventHandler( KeyMappingWindow.bindingButtonName, SystemData.Events.L_BUTTON_UP_PROCESSED, "KeyMappingWindow.OnExitBindingMode" )
    WindowRegisterEventHandler( KeyMappingWindow.bindingButtonName, SystemData.Events.M_BUTTON_UP_PROCESSED, "KeyMappingWindow.OnExitBindingMode" )
    WindowRegisterEventHandler( KeyMappingWindow.bindingButtonName, SystemData.Events.R_BUTTON_UP_PROCESSED, "KeyMappingWindow.OnExitBindingMode" )
    WindowRegisterCoreEventHandler( "KeyMappingWindowActionsList", "OnMouseWheel", "KeyMappingWindow.OnExitBindingModeMouseWheel" )
    
    CallingSetup.UpdateBindingTexts()
    ButtonSetText(KeyMappingWindow.bindingButtonName, L"")
end

function CallingSetup.OnRawDeviceInput( deviceId, itemId, itemDown )
    --DEBUG( L"DeviceId: "..deviceId..L" ItemId: "..itemId..L" ItemDown: "..itemDown..L" ActiveWindow: "..towstring(SystemData.ActiveWindow.name) )
    -- do not allow the binding of left and right mouse buttons
    if( deviceId == 2 and (itemId == 0 or itemId == 1) )
    then
        return
    end
    
    -- Get the button info of the pressed button
    if( KeyMappingWindow.bindMode and itemDown )
    then
        -- Don't put two of the "same" modifier keys in the table
        if( IsOppositeModifierKeyInTable( KeyMappingWindow.selectedKeys, itemId ) )
        then
            return
        end
        
        KeyMappingWindow.selectedKeys[itemId] = modiferKeyTableOrder[itemId]
        if( KeyMappingWindow.selectedKeys[itemId] == nil )
        then
            KeyMappingWindow.selectedKeys[itemId] = KEY_ORDER_LAST
        end
        
        local buttonName = GetButtonName( deviceId, itemId )
        ButtonSetText( SystemData.ActiveWindow.name, buttonName )
        if( KeyMappingWindow.bindingKeyName ~= L"" )
        then
            KeyMappingWindow.bindingKeyName = KeyMappingWindow.bindingKeyName..L" + "
        end
        
        KeyMappingWindow.bindingKeyName = KeyMappingWindow.bindingKeyName..buttonName
        
        -- if it was not a modifier key then bind it and exit binding mode!
        if( not IsModifierKey( itemId ) )
        then
            local buttons = {}
            for key, value in pairs( KeyMappingWindow.selectedKeys ) do 
                table.insert( buttons, key )
            end
            table.sort( buttons, CompareKeyOrder )
            
            -- Remove the old binding
            if( KeyMappingWindow.actionsData[ KeyMappingWindow.SelectedActionDataIndex ]["keys"..KeyMappingWindow.bindIndex].deviceId ~= 0 )
            then
                local boundKeyData = KeyMappingWindow.actionsData[ KeyMappingWindow.SelectedActionDataIndex ]["keys"..KeyMappingWindow.bindIndex]
                RemoveBinding( CallingKeybind.action, boundKeyData.deviceId, boundKeyData.buttons)
            end
            
            -- Add the bindings
            AddBinding( CallingKeybind.action, deviceId, buttons )
            
            -- Refresh the list
            UpdateSingleBindingForAction( CallingKeybind.action, KeyUtils.GetBindingName( buttons, deviceId ) )
            UpdateActionList()
            
            -- Mark has changed so this saves
            KeyMappingWindow.hasChanged = true
            
            -- Reset the button and unregister the events
            ButtonSetPressedFlag( KeyMappingWindow.bindingButtonName, false )
            ButtonSetStayDownFlag( KeyMappingWindow.bindingButtonName, false ) 
            ButtonSetCheckButtonFlag( KeyMappingWindow.bindingButtonName, false )
            KeyMappingWindow.bindMode = false
            KeyMappingWindow.bindingKeyName = L""
            LabelSetText( "KeyMappingWindowHelpText", GetString( StringTables.Default.TEXT_KEYBINDING_HELP ) )
            
            WindowUnregisterCoreEventHandler( KeyMappingWindow.bindingButtonName, "OnRawDeviceInput" )
            WindowUnregisterEventHandler( KeyMappingWindow.bindingButtonName, SystemData.Events.L_BUTTON_UP_PROCESSED )
            WindowUnregisterEventHandler( KeyMappingWindow.bindingButtonName, SystemData.Events.M_BUTTON_UP_PROCESSED )
            WindowUnregisterEventHandler( KeyMappingWindow.bindingButtonName, SystemData.Events.R_BUTTON_UP_PROCESSED )
            WindowUnregisterCoreEventHandler( "KeyMappingWindowActionsList", "OnMouseWheel" )
            
            KeyMappingWindow.bindingButtonName = ""
            
            BroadcastEvent( SystemData.Events.USER_SETTINGS_CHANGED )
            
            CallingSetup.UpdateBindingTexts()
        end
    end
end
