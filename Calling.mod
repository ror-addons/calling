<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="Calling" version="1.0.2" date="23/08/2009" >
		<VersionSettings gameVersion="1.3.1" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Author name="Narketia" email="narketia [a] gmx.net" />
		<Description text="Allows players to call out their target and to assists the player that called the last target" />
		
		<Dependencies>
			<Dependency name="EA_ChatWindow" />
			<Dependency name="EA_MacroWindow" />
			<Dependency name="EA_Window_Help" />
			<Dependency name="EA_KeyMappingWindow" />
		</Dependencies>
		
		<SavedVariables>
			<SavedVariable name="CallingSettings" />
		</SavedVariables>
		
		<Files>
			<File name="Calling.xml" />
			<File name="CalljoinGUI.xml" />
			<File name="MacroUtilites.lua" />
			<File name="Calling.lua" />
			<File name="CallingKeybinding.lua" />
			<File name="CallingSetup.lua" />
			<File name="CallingNotification.lua" />
			<File name="CallingChat.lua" />
			<File name="CallingLocalization.lua" />
			<File name="CallingLocalization-ger.lua" />
			<File name="CallingLocalization-eng.lua" />
			<File name="CallingLocalization-rus.lua" />
			<File name="CallingTargetMarker.lua" />
			<File name="CallingTargetID.lua" />
			<File name="CallingList.lua" />
			<File name="CallingTests.lua" />
			<File name="CallingEvents.lua" />
		</Files>
			
		<OnInitialize>
			<CallFunction name="Calling.Initialize" />
		</OnInitialize>

		<OnUpdate>
			<CallFunction name="Calling.OnUpdate" />
			<CallFunction name="CallingTests.OnUpdate" />
		</OnUpdate>
			
		<OnShutdown/>

		<WARInfo>
			<Categories>
				<Category name="RVR" />
				<Category name="GROUPING" />
			</Categories>
				<Careers>
				<Career name="BLACKGUARD" />
				<Career name="WITCH_ELF" />
				<Career name="DISCIPLE" />
				<Career name="SORCERER" />
				<Career name="IRON_BREAKER" />
				<Career name="SLAYER" />
				<Career name="RUNE_PRIEST" />
				<Career name="ENGINEER" />
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
				<Career name="WITCH_HUNTER" />
				<Career name="KNIGHT" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="CHOSEN" />
				<Career name= "MARAUDER" />
				<Career name="ZEALOT" />
				<Career name="MAGUS" />
				<Career name="SWORDMASTER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="WHITE_LION" />
				<Career name="ARCHMAGE" />
			</Careers>
		</WARInfo>
	</UiMod>
</ModuleFile>
