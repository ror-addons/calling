This plugin allows players to dynamically call for assists.
For greater flexibility the plugin uses a user-specified chat channel for the necessary communication.



--Slash Commands--

* /calling - shows the calling setup window
* /calljoin [Channel] - creates or joins the given channel for calling
* /callquit - leaves the current calling channel
* /callmark - toggles marking the called target with a warhammer sign (scans all incoming combat messages - performance)



--Installing - Short--

If you are new to WAR plugins read Installing - Long at the end of this document, please.

ONCE , after installing:

* Make sure you have installed the "LibSlash" plugin, available on war.curse.com
* Unzip the Calling plugin to your Addons folder
* In WAR: Follow the instructions in the Calling setup window (shown through "/calling")

For advanced or impatient users this should suffice to get started.
I hope you enjoy the plugin!

For a more detailed description read on, please.



--Installing - Long --

Copy the Calling folder into the subfolder "Interface\AddOns" of your warhammer directory. Create those folders if necessary.
Example location: "C:\Games\Warhammer Online - Age of Reckoning\Interface\AddOns\Calling"

If you don't already have the plugin "LibSlash", download and install it.

By restarting Warhammer or using the chat-command "/reloadui" you can activate the plugin. There should be messages in the chat window saying "CALL: Calling initialized" and "CALL: Slash-commands registered" after doing so.

Pull the macros from the "Calling Setup Screen" (shown by entering "/calling" in the chat window) into one of the action bars to be able to call and target just as if you were using a skill.

After doing so, you can set hotkeys for the macros (or any other skill) using the Calling Setup window. To do this first select the skill you want to set the hotkey for. The shortcut can be set by clicking on the according button in the calling setup screen followed by entering the shortcut.
Note: The hotkeys are bound to the up to 4 (physical) skillbars. If you put the macro into one of the logical skillbars (selected by using the arrows in the physical skillbar), the shortcut will be set for the physical skillbar in which the logical one is normally shown.   



--Usage-Setup - Detailed--

For being able to call, you need to be in the same channel as the players you want to coordinate with.

* Use "/calljoin [ChannelName]" in the chat window to join a calling channel.
ChannelName needs to be the same for all players you coordinate with.
Example: "/calljoin TopSecretChannel"

* If you don't want to receive calls anymore, type "/callquit"



--Usage-Calling--

To "advertise" your target, use the Call-Macro, you dragged into the action bar.
If you use the Target macro, you will assist the last player who issued a call.


--Version history--

1.0.2

* Most of the changes in this update have been provided by Gogijan.
	THANKS FOR THIS, GOGIJAN!
* Color of the Calling notification is now dependant on target's class (Gogijan)
	* This feature can be toggled on and of from Setup->Visibility
* Calls are now withdrawn if the caller assists someone else (gogijan)
* The addon should now work in unicode clients of warhammer (Gogijan)
* A russion localization is available in unicode clients (Gogijan)
	=> !!! Party invitations are incompatible with Calling 1.0.1 and lower !!!
	
* You can now priorize calls from within your party, warband or scenario group!
	* This is especially usefull for premade scenario groups!
* Calling notification and the accompanying icons can now be moved and resized
* Some minor appearance tweaks

Developer Information:
* Test cases for ensuring some core functionality of the plugin has been added (CallingTests.lua)
	* enter "/script CallingTests:Run()" into the chat line to execute them
* Added event handling methods (CallingEvents.lua)

1.0.1

* the addon language can be selected in CallingSetup/Visibility
* if active language is selected and not localized, english is used as default
* fixed a bug ocurring regularly when target marking is enabled

1.0

* fixed a bug, that made the plugin crash on initialization if LibCombatLog was not activated
* added the possibility to remove entries from the calls and kills lists by right-clicking on them
* added documentation throughout the whole codebase

0.7.2

* changed the way group-members are recognized in order to reduce the usage of /channelwho
** channeljoiners and leavers send a message
** each chatter in the calling channel is added to user list
** only on unannounced join/quit messages of users on list a update is triggered
* there is now an option to toggle withdrawing of NPC an PC calls
* removed some spam from the debug messages
* slightly varied the timeouts for sending chat-messages
* added a Tutorial-Section to Calling Setup

0.7.1

* the Calling button is now movable
* fixed some badly or not at all localized strings
* fixed a small bug, producing error messages on startup
* invitations and invite requests will now be processed for all localized languages and not just for the active one
* tried to remove clutter from Calling Setup und make it better understandable


0.7

* calls on NPC are automatically withdrawn if the caller has them in target or mouseovers them when they are dead
* calls on enemy players are automatically removed from the list, as soon his dead is declared in the combat log (TODO: english localization)
* the number of assisters (with v0.6.1+) is now tracked
(* added LibCombatLog to build a unified and efficient way to process messages from the combat log)
* added a kill window, showing the last few kills on called targets
* on click on one of the call-kills, a bragging text is inserted into the chat line, ready to being sent
* There is now an icon left of the (ea-standard-)minimap, toggling the calling setup window on and off

* the visibility of the calls and kills list can be toggled from within calling setup
* the visibility of caller and target career icons can be toggled from calling setup
* target marking can be toggled in calling setup (could have performance drawbacks, if on)
* evaluating rvr-kills in order to remove killed targets can be toggled in calling setup (could have performance drawbacks, if on)

* the calljoin command is no longer necessary, as leftclicking the calling icon now opens callgroup management:
	* if not in calling group you can either create one or ask for existing ones
	* if in calling group you can leave it or invite fellows from party,warband,guild,alliance or local
	* settings are available which invitations or requests for invitations should be processed
	
* fixed a bug, showing the kills without time and number of assists, after rejoining a calling group
* calls list is now cleared upon leaving or changing the calling group
* the calling messages are now automatically filtered out of the standard chat window, as long as you are in a calling channel
* The calls list header now shows the number of players in the channel
* the amount and list of players in the calling group is now automatically updated
* LibSlash is now optional

0.6

* IMPORTANT: now creating realm-dependent scenario channels. this means v0.5.1- and v0.6+ do not end up in the same scenario channel
* each user can now configure call-priorities via the /calling menu
* lower priority calls can still be accessed through toggling with the target macro
* fixed a bug that blocked further /channeljoin commands if a channeljoin fails
* your calls will now be withdrawn if calling a dead enemy or issuing a call without enemy target

0.5.1

* the class of the caller is now shown together with the call
* there is now a list, showing the last 3 calls
* the target macro does now toggle through the list, if repeatedly used within intwervals below 1 second
* several fixes on the target marker code
* added a first implementation of getting all users from the calling channel (mouseover caller in list window)

0.5

* if the called target is a player, it's class will show up together with the call
* fixed a bug with keeping the correct /callmark value from session to session
* splitted the codebase to different files matching the functionality of the code
* fixed a bug that caused /callquit not to work
* extracted all localizable strings to a separate file
* localized the strings for the german version (english localization for damage parsing still missing)
* plugin now prevents chat-messages to be stopped by the spam filter
* calljoin now correctly leave previous call channel, if there was one
* on entering a scenario, you join a scenario-wide calling channel. on leaving you rejoin the previous one
* calling notification window is now movable and resizable

0.4

* added command /callmark, which toggles between marking the target in the ui and not doing so
* marker will only be shown on units, which have previously been mouseovered or been damaged by the player
* first upload on curse

0.3.4

* fixed a bug occuring when the badword filter was enabled (needs caller to have new version)
* calling channels are not case sensitive anymore


0.3.3

* fixed a bug which removed the display of a clock-plugin
* cleaned up the code a bit

0.3.2

* added a field to allow for hotkey binding right from the calling setup window

0.3.1

* the plugin automatically registers call and join macros now
* via "/calling" a setup window can be shown, containing the two calling macros ready to be dragged into the action bar

0.3

* adapted the plugin to the patch that disallows scripts to use "/target"
* now uses user-defined channels to communicate calls ("/callchan" and "/callquit")

To be removed again:
* the open party window will show up, when in a warband
* when in a warband, the "join group" button invites the group into the warband (you need to be Warband leader for this to work)

0.2

* added direct targeting support for players
* color of call text is now red for players and green for mobs

0.1

Alpha Version

--Contact--

If you have feedback
email: narketia [a] gmx.net
ingame: Narketia/Gorendil on german Server Erengrad (account inactive)
