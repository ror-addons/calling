-- Copyright (c) 2009, Kristian Bergmann
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without
-- modification, are permitted provided that the following conditions are met:
--
--   * Redistributions of source code must retain the above copyright notice,
--     this list of conditions and the following disclaimer.
--   * Redistributions in binary form must reproduce the above copyright
--     notice, this list of conditions and the following disclaimer in the
--     documentation and/or other materials provided with the distribution.
-- 
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
-- AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
-- IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
-- ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
-- LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
-- CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
-- SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
-- INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
-- CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
-- ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
-- POSSIBILITY OF SUCH DAMAGE.

--Definition of the english localization of the Calling addon.
--In order to define the localization for a  different language, copy this file
--and change the language identifier in the next line as well as all strings.
--Placeholders (in parantheses) need to stay in the given amount and order.
Calling.localization[SystemData.Settings.Language.ENGLISH] = {
	--used for parsing damage reports on enemies from combat log
	hitPhrases = {
		{L"Your (.+) critically hits (.+) for (.*)", function (s,e,r) return e end },
		{L"Your (.+) hits (.+) for (.*)", function (s,e,r) return e end },
	},
	
	--used for parsing pet's damge reports from combat log
	petPhrases = {
		{L"Your pet's (.+) critically hits (.+) for (.*)", function (s,e,r) return e end },
		{L"Your pet's (.+) hits (.+) for (.*)", function (s,e,r) return e end },
	},

	--used to parse incoming damage from combat log
	dmgPhrases = {
		{L"(%a+)'s (.*) critically hits you for (.*)", function (e, s, r) return e end},
		{L"(%a+)'s (.*) hits you for (.*)", function (e, s, r) return e end}
	},
	
	--used to parse kill messages from combat log
	killPattern = L"(.*) has been (.*) by (.*).(.*)",
	
	addonname=L"CALL",
	notificationWindowCaption = L"Calling Notification",
	notificationWindowDescription = L"Displays the last caller, his target name and the target's career.",
	listWindowCaption = L"Calling List",
	listWindowDescription = L"Displays a list with the last few calls.",
	killsWindowCaption = L"Calling-Kills List",
	killsWindowDescription = L"Displays a list with the last few kills on called targets.",
	callingIconCaption = L"Calling Icon",
	callingIconDescription = L"Button for managing Calling groups and showing the Calling setup",
	callInited = L"Calling initialized",
	noLibSlashFeatures = L"If you want to use the slash commands of \"Calling\" please install the plugin \"LibSlash\".",
	noLibCombatLogFeatures = L"If you want enemies killed in RvR to disappear from the Calls-List automatically, you need to activate the plugin \"LibCombatLog\".",
	callSlashInited = L"Slash-commands registered",
	useCalljoin = L"! Use the group management options of the Calling icon first !",
	callLabel = L" calls ",
	chanLeft = L"Left calling group: ",
	calljoinUsage = L"usage: /calljoin CallGroupName",
	chanJoined = L"Joining calling group: ",
	markEnabled = L"Target marker enabled",
	markDisabled = L"Target marker disabled",
	dmgText = L" damage",
	autoEnabled = L"Auto Assisting Enabled",
	autoDisabled = L"Auto Assisting Disabled",

	subMenus = L"Sub Menus",
	
	macros = L"Macros",
	macroInstructions = L"Drag&Drop the two macros into your skill-bar in order to use calling.",
	
	keybindings = L"Key Binding",
	boundKey = L"Bound",
	bindSelectInstructions = L"In order to select hotkeys for a macro, select it in your skill-bar by right-clicking on it.",
	bindClickInstructions = L"Click on the button below in order to choose a keybinding.",
	bindInstructions = L"Enter the desired shortcut or click on the button again in order to cancel.",
	noSlotSelected = L"No skill-slot selected.",
	selectSlot = L"Select Slot",
	selectSlotToProceed = L"Select a skill-bar slot to proceed.",
	selectedSlot = L"Selected skill-slot: ",
	inBar = L" in bar ",
	
	priorities = L"Priorities",
	prioInstructions = L"This priorities determine the importance of different calls.\nIf a call falls into a high priority it will appear closer to the top of the call-list.\nIf a new call's priority is as high as the current one's it becoms the current call.",
	prio1 = L"Highest priority",
	prio2 = L"2nd priority",
	prio3 = L"3rd priority",
	type = L"Type",
	selection = L"Selection",

	display = L"Display",
	showGroupIcons = L"Show Group Icons",
	showCallList = L"Show Calls list",
	showKillList = L"Show Kills list",
	showCallerClass = L"Show caller's career",
	showTargetClass = L"Show target's career",
	notificationCareerColor = L"Target's career -> call color",
	language = L"Language",
	languageName = L"English",
	activeLanguage = L"Ingame language",

	targeting = L"Targeting",
	autoAssist = L"Auto Assisting",
	showTargetMarker = L"Show target marker (performance)",
	evaluateRvrKills = L"Remove Player calls on death (performance)",
	removeNpc = L"Remove NPC calls on death",
	
	groupMgmt = L"Group Management",	
	inviteDesc = L"Dialog on Calling group invitation:",
	showGroupInvites = L"Show",
	showInvitesWhenGrouped = L"Supress if in Calling group",
	inviteOnRequestDesc = L"Send invitation on request:",
	reactOnInvitationRequestsGnA = L"Requests from Guild+Alli",
	reactOnInvitationRequestsPnW = L"Requests from group+WB",
	reactOnInvitationRequestsL = L"Requests from local chat",		

	age = L"Age",
	caller = L"Caller",
	target = L"Target",
	lived = L"Time",
	killedTarget = L"Slain",
	clickToAssist = L"Click to assist ",
	versus = L" on ",
	rightClickToRemove = L"Removes this entry from the list.",
	updating = L"--updating--",
	userList = L"Caller List",
	
	priorityTypes = {
		[Calling.Priority.CALLER] = L"Caller",
		[Calling.Priority.CALLER_TYPE] = L"Caller Archetype",
		[Calling.Priority.CALLER_CAREER] = L"Caller Career",
		[Calling.Priority.TARGET_TYPE] = L"Target Archetype",
		[Calling.Priority.TARGET_CAREER] = L"Target Career",
		[Calling.Priority.CALLER_GROUP] = L"Caller Group",
		[Calling.Priority.NONE] = L"-None-"
	},
	
	characterTypes = {
		[Calling.Archetypes.MELEE_DMG] = L"Melee-Damage",
		[Calling.Archetypes.RANGED_DMG] = L"Ranged-Damage",
		[Calling.Archetypes.HEALER] = L"Healer",
		[Calling.Archetypes.TANK] = L"Tank"
	},
	
	groupTypes = {
		[Calling.Grouptypes.PARTY] = L"Party",
		[Calling.Grouptypes.WARBAND] = L"Warband",
		[Calling.Grouptypes.SCENARIO_GROUP] = L"Scenario Group"
	},

	choosePrioType = L"Choose priority type",
	
	version06Warning = L"CALLING: Scenario-Callgroups changed! Remind your comrades-in-arms to install Calling v0.6+ in order to coordinate!",
	timePart1 = L" (",
	timePart2 = L" ago)",
	
	braggingTime = L" has been killed ",
	braggingCaller = L" after receiving the call from ",
	braggingAssisters = L", aided by  ",
	braggingKilled = L" assisters.",
	clickToInsert = L"Click in order to insert the following text into the chat input line: ",
	groupManagement = L"LM: Join, leave and manage Calling group",
	settingsShownToggle = L"RM: Show/Hide the Calling setup window",
	groupFounder = L"Calling group: ",

 	noChannelOptions = L'"Create" creates a Calling group named %q. Using "Request" you can ask for open Calling-Groups.',
	cancel = L"Cancel",
	request = L"Request",
	create = L"Create",
	inChannelOptions = L'Using "Invite" you can invite fellows to join your Calling group. "Leave" leaves your Calling group.',
	invite = L"Invite",
	leave = L"Leave",
	inviteInstructions = L"Choose the chat-channel you want to address the invitation to from the dropdown list and confirm, please.",
	requestInstructions = L"Choose the chat-channel from the dropdown list in which you want to request an invitation and confirm, please.",
	channelAdvertise = {
		form = L'I am in the Calling group %q. Join me with /calljoin %q, if you have the addon.',
		pat = L'I am in the Calling group "(.*)". Join me with /calljoin "(.*)", if you have the addon.'
	},
	channelAdvertGotten = L'%q invited you to join the Calling group %q.',
	join = L"Join",
	ignore = L"Ignore",	
	channelRequest = {
		form = L'Does anyone have a Calling group open?',
		pat = L'Does (.*) have a Calling group open?'
	},
	guild = L"Guild",
	alliance = L"Alliance",
	group = L"Group",
	warband = L"Warband",
	local_ = L"Local",
	whereToBroadcast = L"To which chat-channel shall the Calling-group invitation be sent?",
	
	tutorial = L"TUTORIAL",
	tutDescription = L"Calling provides a flexible usage of WARs assist mechanic and adds some convenience to it.",
	tutParts = {
		{Title =  L"Setup",
			Text = L"The only setup necessary is to move the macros \"Call\" and \"Target\" into your skill-bar.\nSelect the \"Macros\" sub menu and Drag'n'Drop the two macros into your skill bar."},
		{Title =  L"Joining",
			Text = L"Before you can focus damage by calling, you need to gather within a Calling group.\nClick the Calling button left of the minimap in order to form and manage Calling groups."},
		{Title =  L"Scenarios",
			Text = L"When joining a scenario, you will be put into a Calling group with all other players in the scenario automatically .\nWhen leaving the scenario, you return to your previous Calling group."},
		{Title =  L"Call",
			Text = L"You can call your target by using the \"Call\" macro.\nThis will pop up a notification on the screens of your Calling group's members."},
		{Title =  L"Target",
			Text = L"If you get notified that someone called a target, you can use the \"Assist\" macro in order to target the caller's current enemy.\nIf the caller switched to another target than the called one, you will assist on the new target."},
		{Title = L"Customizing",
			Text = L"If you want to move or resize the calls- or kills-lists, the Calling button or the Calling notification then choose \"Customize Interface\" from the main menu and click on \"Layout editor...\" afterwards."},
		{Title =  L"Visibility",
			Text = L"You can hide the calls- and kills-lists using the \"Display\" submenu.\nRight-clicking the Calling button toggles the visibility of Calling Setup."}
	},
}
